program test_ozaki

  use ozaki_integrator
  use green_1d, only: green
  use constants, only: PAULI_X, PAULI_Y, PAULI_Z
  use math, only: vector3D, IM, RE, CZERO
 
  integer, parameter :: N = 4 
  integer, parameter :: M = 1 
  type(vector3D) :: cspins(N), seg(N)
  double complex :: rho(2*N, 2*N)
  double complex :: rho_ij(M)
  character (len=50) :: fstring
  real :: energy
  integer :: i
 
  call init_ozaki(300)

  do i = 1, N
!    cspins(i)%x = 1.0d0 / cosh(1.0*(N/2 - i))
!    cspins(i)%y = 0.0
!    cspins(i)%z = 1.0d0 * tanh(1.0*(N/2 - i))
    cspins(i)%x = 0.0d0
    cspins(i)%y = 0.0
    cspins(i)%z = 1.0d0
  enddo 

  seg = spindensity_eq(cspins, 1.80, t=1.0, temperature=300.0, jsd=0.1)
  
  open(unit=22, file='seq.txt', status='replace', action='write')
  write(22, '(66ES20.10E3)') seg
  close(22)

  print *, "calculating rho"

  open(unit=22, file='rho.txt', status='replace', action='write')
  do i = 1, M 

!    energy = -3.0 + i*(6.0/M)

    energy  = 0.1

    print *, energy 
    rho(:, :) = rho_ozaki(green, energy, t=1.0, spins=cspins, &
                          temperature=300.0, jsd=0.1)
!    rho(:, :) = green(cspins, energy*RE, t=1.0*RE, j_sd=0.1d0) 
    write(fstring, '(A, I, A)') '(', 2*N*2*N+1, 'ES20.10E3)'
    write(22, trim(fstring)) energy, real(rho)
  end do

  print *, "Done with rho"

  close(22)
    
  print *, PAULI_X
  print *, PAULI_Y
  print *, PAULI_Z


    
  print *, "Hello world"
    

end program test_ozaki

