program test_llg

 use math, only: vector3D
 use llg
 use input_output, only: clear_file

 type(vector3D) :: cspins(10)
    
 cspins = vector3D(1, 2, 3)
 
 call clear_file('test_output.txt')
 call save_spins(0.1d0, cspins, 'test_output.txt')
    



end program test_llg
