!********************************subroutines for constructing the poles of the Ozaki sum decomposition of the Fermi function********************************



SUBROUTINE const_mat()
   
! This subroutine constructs the matrix used to compute the poles of the Ozaki sum decomposition of the Fermi function 
  
     USE global, only:  Mat_L, Mat_R, N_poles 
     implicit none
     double precision::       x
     integer::                i,j, io_error
   

     do i=1,2*N_poles 
       x=i
       do j=1,2*N_poles           
           if(i .EQ. j) then
           Mat_L(i,j)=0.d0
           end if 
           if(i .EQ. j-1) then
           Mat_L(i,j)=1/(2*sqrt(4*x**2-1)) 
           end if 
           Mat_L(j,i)=Mat_L(i,j)
       end do
     end do

     do i=1,2*N_poles 
       x=i
       do j=1,2*N_poles           
           if(i .EQ. j) then
           Mat_R(i,j)=0.d0
           end if 
           if(i .EQ. j-1) then
           Mat_R(i,j)=1/(2*sqrt(4*x**2-1)) 
           end if 
           Mat_R(j,i)=Mat_R(i,j)
       end do
     end do

     
END SUBROUTINE const_mat




SUBROUTINE construct_nu()

USE global
implicit none
integer::     i,j


     j=1
     DO i=1,2*N_poles 
       IF(Eig_val_mat_L(i) .GT. 0) THEN
         Nu_L(j)=Eig_val_mat_L(i)
         R_L(j)=R_alpha_L(i)
         j=j+1
       END IF
     END DO

     j=1
     DO i=1,2*N_poles 
       IF(Eig_val_mat_R(i) .GT. 0) THEN
         Nu_R(j)=Eig_val_mat_R(i)
         R_R(j)=R_alpha_R(i)
         j=j+1
       END IF
     END DO
 
 
END SUBROUTINE construct_nu


subroutine calc_R()

     use global
     implicit none
     ! after the call to dsysev which diagonalizes Mat_L/R, their eigenvectors are stored columnwise within themselves. 
     ! No need to have previous Eig_vect_mat_L/R. 
          
     integer::  i
    
     do i=1,2*N_poles 
       R_alpha_L(i) = Mat_L(1,i)**2 / (4.d0*Eig_val_mat_L(i)**2)
       R_alpha_R(i) = Mat_R(1,i)**2 / (4.d0*Eig_val_mat_R(i)**2)
     end do  
  
end subroutine calc_R



!**********************************************************************************************************************************************************
 
