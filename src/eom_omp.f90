SUBROUTINE eom(t,vec,dvec)
!******************* eom ************************************************************************************
!		computes the equation of motion needed for the Runke-Kutta method
!************************************************************************************************************
 	

        USE global
	implicit none
	double precision :: t

	double complex, dimension(length_rkvec) :: vec, dvec
	integer :: i,j,k,p,l,m
	
        double complex, dimension(2*n,2*n):: prov1, prov2
		
        call vector_to_matrix_parallelized(vec)   
        

!>>>>>>>>>>>>>>>>>>>>                  


!$OMP PARALLEL DO PRIVATE(k,i,p,j)
        DO k=1,n_lorentz 
          DO i=1,n_channels
             DO p=1,n_lorentz
               DO j=1,n_channels             
 
                    dOmega_LL1(j,p,i,k) = dot_product( csi_L(:,j,p) , psi_L(:,i,k) ) * ( Gam_greater_L_m(j,p) - Gam_lesser_L_m(j,p) )    &
                                        + dot_product( psi_L(:,j,p) , csi_L(:,i,k) ) * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &
				                        - im * (1.d0/hbar) * ( hi_left_m(p) + delta_tdep_L - (hi_left_p(k) + delta_tdep_L) ) * Omega_LL1(j,p,i,k)				       

                    dOmega_LR1(j,p,i,k) = dot_product( csi_R(:,j,p) , psi_L(:,i,k) ) * ( Gam_greater_R_m(j,p) - Gam_lesser_R_m(j,p) )    &
                                        + dot_product( psi_R(:,j,p) , csi_L(:,i,k) ) * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &
			     	                    - im * (1.d0/hbar) * ( hi_right_m(p) + delta_tdep_R - (hi_left_p(k) + delta_tdep_L) ) * Omega_LR1(j,p,i,k)
			     	                    
		    dOmega_RR1(j,p,i,k) = dot_product( csi_R(:,j,p) , psi_R(:,i,k) ) * ( Gam_greater_R_m(j,p) - Gam_lesser_R_m(j,p) )    &
                                        + dot_product( psi_R(:,j,p) , csi_R(:,i,k) ) * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &
                                        - im * (1.d0/hbar) * ( hi_right_m(p) + delta_tdep_R - (hi_right_p(k) + delta_tdep_R) ) * Omega_RR1(j,p,i,k)  
  
                    dOmega_RL1(j,p,i,k) = dot_product( csi_L(:,j,p) , psi_R(:,i,k) ) * ( Gam_greater_L_m(j,p) - Gam_lesser_L_m(j,p) )    &
                                        + dot_product( psi_L(:,j,p) , csi_R(:,i,k) ) * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &
				                        - im * (1.d0/hbar) * ( hi_left_m(p) + delta_tdep_L - (hi_right_p(k) + delta_tdep_R) ) * Omega_RL1(j,p,i,k)                    
			     	        
                END DO
             END DO
           END DO
        END DO
!$OMP END PARALLEL DO          
        
        
!$OMP PARALLEL DO PRIVATE(k,i,p,j)        
        DO k=1,n_lorentz
           DO i=1,n_channels
             DO p=1,k_poles-n_lorentz
                DO j=1,n_channels             
                
                    dOmega_LL2(j,p,i,k) = dot_product( csi_L(:,j,p+n_lorentz) , psi_L(:,i,k) ) * ( Gam_greater_L_m(j,p+n_lorentz) - Gam_lesser_L_m(j,p+n_lorentz) )    &
                                        + dot_product( psi_L(:,j,p+n_lorentz) , csi_L(:,i,k) ) * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &
                                        - im * (1.d0/hbar) * ( hi_left_m(p+n_lorentz) + delta_tdep_L - (hi_left_p(k) + delta_tdep_L) ) * Omega_LL2(j,p,i,k)		

                    dOmega_LR2(j,p,i,k) = dot_product( csi_R(:,j,p+n_lorentz) , psi_L(:,i,k) ) * ( Gam_greater_R_m(j,p+n_lorentz) - Gam_lesser_R_m(j,p+n_lorentz) )    &
                                        + dot_product( psi_R(:,j,p+n_lorentz) , csi_L(:,i,k) ) * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &
			     	                    - im * (1.d0/hbar) * ( hi_right_m(p+n_lorentz) + delta_tdep_R - (hi_left_p(k) + delta_tdep_L) ) * Omega_LR2(j,p,i,k)
			     	                    
	            dOmega_RR2(j,p,i,k) = dot_product( csi_R(:,j,p+n_lorentz) , psi_R(:,i,k) ) * ( Gam_greater_R_m(j,p+n_lorentz) - Gam_lesser_R_m(j,p+n_lorentz) )    &
                                        + dot_product( psi_R(:,j,p+n_lorentz) , csi_R(:,i,k) ) * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &
                                        - im * (1.d0/hbar) * ( hi_right_m(p+n_lorentz) + delta_tdep_R - (hi_right_p(k) + delta_tdep_R) ) * Omega_RR2(j,p,i,k)  

                    dOmega_RL2(j,p,i,k) = dot_product( csi_L(:,j,p+n_lorentz) , psi_R(:,i,k) ) * ( Gam_greater_L_m(j,p+n_lorentz) - Gam_lesser_L_m(j,p+n_lorentz) )    &
                                        + dot_product( psi_L(:,j,p+n_lorentz) , csi_R(:,i,k) ) * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &
				                        - im * (1.d0/hbar) * ( hi_left_m(p+n_lorentz) + delta_tdep_L - (hi_right_p(k) + delta_tdep_R) ) * Omega_RL2(j,p,i,k)                    
                        
                END DO
             END DO
           END DO
        END DO
!$OMP END PARALLEL DO


!$OMP PARALLEL DO PRIVATE(k,i,p,j)        
          DO k=1,k_poles-n_lorentz 
            DO i=1,n_channels
              DO p=1,n_lorentz
                DO j=1,n_channels             
                
                    dOmega_LL3(j,p,i,k) = dot_product( csi_L(:,j,p) , psi_L(:,i,k+n_lorentz) ) * ( Gam_greater_L_m(j,p) - Gam_lesser_L_m(j,p) )            &
                                        + dot_product( psi_L(:,j,p) , csi_L(:,i,k+n_lorentz) ) * ( Gam_greater_L_p(i,k+n_lorentz) - Gam_lesser_L_p(i,k+n_lorentz) )         &
 				                        - im * (1.d0/hbar) * ( hi_left_m(p) + delta_tdep_L - (hi_left_p(k+n_lorentz) + delta_tdep_L) ) * Omega_LL3(j,p,i,k)	           
   			       

                    dOmega_LR3(j,p,i,k) = dot_product( csi_R(:,j,p) , psi_L(:,i,k+n_lorentz) )  * ( Gam_greater_R_m(j,p) - Gam_lesser_R_m(j,p) )    &
                                        + dot_product( psi_R(:,j,p) , csi_L(:,i,k+n_lorentz) )  * ( Gam_greater_L_p(i,k+n_lorentz) - Gam_lesser_L_p(i,k+n_lorentz) )  &
			     	                    - im * (1.d0/hbar) * ( hi_right_m(p) + delta_tdep_R - (hi_left_p(k+n_lorentz) + delta_tdep_L) ) * Omega_LR3(j,p,i,k)
			     	                    
	            dOmega_RR3(j,p,i,k) = dot_product( csi_R(:,j,p) , psi_R(:,i,k+n_lorentz) ) * ( Gam_greater_R_m(j,p) - Gam_lesser_R_m(j,p) )    &
                                        + dot_product( psi_R(:,j,p) , csi_R(:,i,k+n_lorentz) ) * ( Gam_greater_R_p(i,k+n_lorentz) - Gam_lesser_R_p(i,k+n_lorentz) )    &
                                        - im * (1.d0/hbar) * ( hi_right_m(p) + delta_tdep_R - (hi_right_p(k+n_lorentz) + delta_tdep_R) ) * Omega_RR3(j,p,i,k)  
   
                    dOmega_RL3(j,p,i,k) = dot_product( csi_L(:,j,p) , psi_R(:,i,k+n_lorentz) ) * ( Gam_greater_L_m(j,p) - Gam_lesser_L_m(j,p) )    &
                                        + dot_product( psi_L(:,j,p) , csi_R(:,i,k+n_lorentz) ) * ( Gam_greater_R_p(i,k+n_lorentz) - Gam_lesser_R_p(i,k+n_lorentz) )    &
				                        - im * (1.d0/hbar) * ( hi_left_m(p) + delta_tdep_L - (hi_right_p(k+n_lorentz) + delta_tdep_R) ) * Omega_RL3(j,p,i,k)                    
			     	                    
                END DO
             END DO
           END DO
        END DO
!$OMP END PARALLEL DO        
        
        

        
!>>>>>>>>>>>>>>>>>>>>

!OMP PARALLEL
!OMP WORKSHARE
    summ1_L(:,:,:) = dcmplx(0.d0,0.d0)
    summ2_L(:,:,:) = dcmplx(0.d0,0.d0)
    summ1_R(:,:,:) = dcmplx(0.d0,0.d0)
    summ2_R(:,:,:) = dcmplx(0.d0,0.d0)
    summ3_L(:,:,:) = dcmplx(0.d0,0.d0)
    summ3_R(:,:,:) = dcmplx(0.d0,0.d0)
!OMP END WORKSHARE
!OMP END PARALLEL 

     
        DO k=1,n_lorentz
          DO i=1,n_channels
             DO l=1,2*n
                     
               DO p=1,n_lorentz
                  DO j=1,n_channels
                     summ1_L(l,i,k) = summ1_L(l,i,k) + (1.d0/hbar) * Omega_LL1(j,p,i,k) * csi_L(l,j,p) + (1.d0/hbar) * Omega_LR1(j,p,i,k) * csi_R(l,j,p)  
                     summ1_R(l,i,k) = summ1_R(l,i,k) + (1.d0/hbar) * Omega_RR1(j,p,i,k) * csi_R(l,j,p) + (1.d0/hbar) * Omega_RL1(j,p,i,k) * csi_L(l,j,p)
                  END DO 
               END DO
 
 
               DO p=1,k_poles-n_lorentz
                  DO j=1,n_channels
                      summ2_L(l,i,k) = summ2_L(l,i,k) + (1.d0/hbar) * Omega_LL2(j,p,i,k) * csi_L(l,j,p) + (1.d0/hbar) * Omega_LR2(j,p,i,k) * csi_R(l,j,p)  
                      summ2_R(l,i,k) = summ2_R(l,i,k) + (1.d0/hbar) * Omega_RR2(j,p,i,k) * csi_R(l,j,p) + (1.d0/hbar) * Omega_RL2(j,p,i,k) * csi_L(l,j,p)  
                   END DO
               END DO

             END DO                     
          END DO
        END DO                   
                   
        
        DO k=1,k_poles-n_lorentz
          DO i=1,n_channels
            DO l=1,2*n
 
              DO p=1,n_lorentz
                 DO j=1,n_channels                 
                     summ3_L(l,i,k) = summ3_L(l,i,k) + (1.d0/hbar) * Omega_LL3(j,p,i,k) * csi_L(l,j,p) + (1.d0/hbar) * Omega_LR3(j,p,i,k) * csi_R(l,j,p)
                     summ3_R(l,i,k) = summ3_R(l,i,k) + (1.d0/hbar) * Omega_RR3(j,p,i,k) * csi_R(l,j,p) + (1.d0/hbar) * Omega_RL3(j,p,i,k) * csi_L(l,j,p) 
                  END DO
              END DO 

            END DO 
          END DO
        END DO  
        

!>>>>>>>>>>>>>>>>>>>>


!$OMP PARALLEL DO PRIVATE(k,i)     
      DO k=1,n_lorentz
          DO i=1,n_channels

              csi_loop(:,1,i,k) = csi_L(:,i,k)
              psi_loop(:,1,i,k) = psi_L(:,i,k)
              csi_loop(:,:,i,k) = matmul(rho(:,:),csi_loop(:,:,i,k))
              psi_loop(:,:,i,k) = matmul( H(:,:),psi_loop(:,:,i,k) )
         
              dpsi_L(:,i,k) = - im * Gam_lesser_L_p(i,k) * csi_L(:,i,k) - im * csi_loop(:,1,i,k) * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &			    
                              - im * (1.d0/hbar) * psi_loop(:,1,i,k) + im * (1.d0/hbar) * ( hi_left_p(k) + delta_tdep_L ) * psi_L(:,i,k) &  
                              - im * (1.d0/hbar) * summ1_L(:,i,k) - im * (1.d0/hbar) * summ2_L(:,i,k)                         
                                
              csi_loop(:,1,i,k) = csi_R(:,i,k)
              psi_loop(:,1,i,k) = psi_R(:,i,k)
              csi_loop(:,:,i,k) = matmul(rho(:,:),csi_loop(:,:,i,k))
              psi_loop(:,:,i,k) = matmul( H(:,:),psi_loop(:,:,i,k) )
              
              dpsi_R(:,i,k) = - im * Gam_lesser_R_p(i,k) * csi_R(:,i,k) - im * csi_loop(:,1,i,k)  * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &			    
                              - im * (1.d0/hbar) * psi_loop(:,1,i,k)  + im * (1.d0/hbar) * ( hi_right_p(k) + delta_tdep_R ) * psi_R(:,i,k) &  
                              - im * (1.d0/hbar) * summ1_R(:,i,k) - im * (1.d0/hbar) * summ2_R(:,i,k)                                                  
                                
          END DO
      END DO 
!$OMP END PARALLEL DO

      
!$OMP PARALLEL DO PRIVATE(k,i)          
      DO k=n_lorentz+1,k_poles
          DO i=1,n_channels

              csi_loop(:,1,i,k) = csi_L(:,i,k)
              psi_loop(:,1,i,k) = psi_L(:,i,k)
              csi_loop(:,:,i,k) = matmul(rho(:,:),csi_loop(:,:,i,k))
              psi_loop(:,:,i,k) = matmul( H(:,:),psi_loop(:,:,i,k) ) 
         
              dpsi_L(:,i,k) = - im * Gam_lesser_L_p(i,k) * csi_L(:,i,k) - im * csi_loop(:,1,i,k)  * ( Gam_greater_L_p(i,k) - Gam_lesser_L_p(i,k) )    &			    
                              - im * (1.d0/hbar) * psi_loop(:,1,i,k) + im * (1.d0/hbar) * ( hi_left_p(k) + delta_tdep_L ) * psi_L(:,i,k) &  
                              - im * (1.d0/hbar) * summ3_L(:,i,k-n_lorentz)
                              
              csi_loop(:,1,i,k) = csi_R(:,i,k)
              psi_loop(:,1,i,k) = psi_R(:,i,k)                
              csi_loop(:,:,i,k) = matmul(rho(:,:),csi_loop(:,:,i,k))
              psi_loop(:,:,i,k) = matmul( H(:,:),psi_loop(:,:,i,k) )
              
              dpsi_R(:,i,k) = - im * Gam_lesser_R_p(i,k) * csi_R(:,i,k) - im * csi_loop(:,1,i,k) * ( Gam_greater_R_p(i,k) - Gam_lesser_R_p(i,k) )    &			    
                              - im * (1.d0/hbar) * psi_loop(:,1,i,k) + im * (1.d0/hbar) * ( hi_right_p(k) + delta_tdep_R ) * psi_R(:,i,k) &  
                              - im * (1.d0/hbar) * summ3_R(:,i,k-n_lorentz) 
                                         
         END DO
     END DO 
!$OMP END PARALLEL DO     
         			
         			
!>>>>>>>>>>>>>>>>>>>>

!OMP PARALLEL
!OMP WORKSHARE
      Pi_L(:,:) = dcmplx(0.d0,0.d0)
      Pi_R(:,:) = dcmplx(0.d0,0.d0)
!OMP END WORKSHARE
!OMP END PARALLEL 


  
           
!$OMP PARALLEL DO PRIVATE(l,m,i,k)           
       DO m=1,2*n   
          DO l=1,2*n
             DO k=1,k_poles
                DO i=1,n_channels   
               
                   prov1(l,m) = (1.d0/hbar) * psi_L(l,i,k) * dconjg(csi_L(m,i,k))  
                   prov2(l,m) = (1.d0/hbar) * psi_R(l,i,k) * dconjg(csi_R(m,i,k))

                   Pi_L(l,m) = Pi_L(l,m) + prov1(l,m)
                   Pi_R(l,m) = Pi_R(l,m) + prov2(l,m)
                END DO 
             END DO
           END DO 
        END DO     
!$OMP END PARALLEL DO        
        
!>>>>>>>>>>>>>>>>>>>>       
        prov1(:,:) = matmul(H(:,:),rho(:,:))
        prov2(:,:) = matmul(rho(:,:),H(:,:))

        !write(*,*) 'elements of rho', rho(1,1), rho(2,2), rho(3,3), rho(4,4)
        !write(*,*) 'elements of H', H(1,1), H(2,2), H(3,3), H(4,4) 
        !write(*,*) 'elements of Pi_L', Pi_L(1,1), Pi_L(2,2), Pi_L(3,3), Pi_L(4,4) 


!OMP PARALLEL
!OMP WORKSHARE

        drho(:,:) = - im * (1.d0/hbar) * ( prov1(:,:) - prov2(:,:)  )&
                    + (1.d0/hbar) * ( Pi_L(:,:) + dconjg(transpose(Pi_L(:,:))) )&
                    + (1.d0/hbar) * ( Pi_R(:,:) + dconjg(transpose(Pi_R(:,:))) )      

!OMP END WORKSHARE
!OMP END PARALLEL  		



call matrix_to_vector_parallelized(dvec)


END SUBROUTINE eom 
