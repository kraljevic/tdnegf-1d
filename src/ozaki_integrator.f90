module ozaki_integrator
   
    use green_1d, only: green
    use math
    use constants, only: KB
    integer :: count_ozaki = 1
    double precision, allocatable, &
        dimension(:) :: poles, residues
    double complex, allocatable, &
        dimension(:) :: poles_denis, res_denis
    double precision, parameter :: PI = 3.14159265359

    interface fermi
        module procedure fermi_rel_arg
        module procedure fermi_im_arg
    end interface


contains

  subroutine spindensity_eq(cspins, energy, t, &
                            temperature, jsd, &
                            spin_eq, rho, solver) 
    ! Compute the spin density in equilibrium using
    ! Ozaki method.
    type(vector3D), dimension(:), intent(in) :: cspins

    type(vector3D), dimension(size(cspins, 1)), &
        intent(out) :: spin_eq  
    character(len=:), allocatable, intent(in) :: solver

    real, intent(in) :: energy, t, temperature, jsd

    double complex, &
        dimension(2*size(cspins, 1), 2*size(cspins, 1)), &
        intent(out) :: rho 
    
    if (solver .eq. 'ozaki') then
        rho = rho_ozaki(green, energy, t, cspins, &
                        temperature, jsd)
    else if (solver .eq. 'denis') then
        rho = rho_denis(green, energy, t, cspins, &
                        temperature, jsd)
    else 
        stop "Unknown equilibrium solver"
    endif 
    spin_eq = spindensity(rho)
  end subroutine



  function rho_ozaki(green_func, energy, t, spins, &
                     temperature, jsd, rdist)
    implicit none
    real, intent(in) :: jsd, temperature, energy, t 
    type(vector3D), dimension(:), intent(in) :: spins
    double precision, optional :: rdist
    double precision :: rdist_in
    
    interface 
      function green_func(cspins, energy, t, j_sd)
        use math
        type(vector3D), dimension(:), intent(in) :: cspins
        double complex, intent(in) :: energy, t
        double precision, intent(in) :: j_sd

        double complex, dimension(2*size(cspins), &
            2*size(cspins)) :: green_func
      end function 
    end interface

    integer :: i, n
    double precision :: beta
    double complex, dimension(2*size(spins), &
                              2*size(spins)) :: rho_ozaki
    double complex, dimension(2*size(spins), &
                              2*size(spins)) :: sum_ozaki
    double complex :: pole
    
    if (count_ozaki .ne. 2) &
        stop "Error! Ozaki integrator not initialized."
    
    if (present(rdist)) then
      rdist_in = rdist
    else
      rdist_in = 1.0e+30
    endif 

    rho_ozaki = 0.0d0
    sum_ozaki = (0.0d0, 0.0d0)
    beta = 1.0 / (KB * temperature)
    n = size(poles)
    do i = 1, n
      pole = energy + IM * poles(i) / beta
      sum_ozaki = sum_ozaki +  &
                  residues(i)* green_func(spins, pole, &
                                          t=RE*t, &
                                          j_sd=1.d0*jsd)
    end do
    sum_ozaki = 2 * IM * sum_ozaki / beta  
    rho_ozaki = (sum_ozaki - conjg(transpose(sum_ozaki))) / &
                (2*IM)
    
    rho_ozaki = rho_ozaki + &
                0.5 * IM * rdist_in * &
                     green_func(spins, IM*rdist_in, &
                                t=RE*t, j_sd=1.d0*jsd)
       
  end function rho_ozaki 
    

  subroutine init_ozaki(number_of_poles)
    if (count_ozaki == 1) then
      allocate(poles(number_of_poles)) 
      allocate(residues(number_of_poles))
      count_ozaki = 2
      call find_poles(number_of_poles, poles, residues)
    else
      stop "Error! Ozaki integrator already set"
    end if
    
  end subroutine init_ozaki
  
 
  subroutine find_poles(n_poles, poles, residues)
    implicit none
    integer, intent(in) :: n_poles
    double precision, dimension(n_poles), &
        intent(out) :: poles, residues
    integer :: n, i
    double precision, &
        dimension(n_poles*2+1, n_poles*2+1) :: ozaki_matrix
    double precision, dimension(n_poles*2+1) :: eigenvalues
    integer :: lwork, info, count_poles
    double precision, dimension(1) :: size_work
    double precision, allocatable, dimension(:) :: work

    ozaki_matrix = 0
    n = 2*n_poles + 1
    do i = 1, n-1
      ozaki_matrix(i, i+1) = 1.0 / (2*sqrt(4.0*i**2 - 1.0))
      ozaki_matrix(i+1, i) = 1.0 / (2*sqrt(4.0*i**2 - 1.0))
    end do
    call dsyev('V', 'U', n, ozaki_matrix, n, eigenvalues, &
               size_work, -1, info) 
    
    lwork = size_work(1)
    allocate(work(lwork))
    eigenvalues = 0.0
    call dsyev('V', 'U', n, ozaki_matrix, n, &
               eigenvalues, work, lwork, info)
    
    count_poles = 1
    do i = 1, n 
      if (eigenvalues(i) > 0) then
        residues(count_poles) = ozaki_matrix(1, i)**2 / &
                                (4*eigenvalues(i)**2)
        poles(count_poles) = 1.0 / eigenvalues(i)
        count_poles = count_poles + 1
      end if
    end do

    ! Reverse the order of elements 
    poles = poles(count_poles-1:2:-1)
    residues = residues(count_poles-1: 2:-1)

  end subroutine find_poles



  subroutine denis_poles(mu, mu_im, mu_re, &
                         temp, temp_im, temp_re, &
                         n, n_im, n_re)
      ! Subroutine used to compute poles and residues 
      ! of the Fermi function expansion based on 
      ! D. Areshkin Phys. Rev. B 81, 1445450 (2010)

    implicit none
    double precision, intent(in) :: mu, mu_im, mu_re, &
                                    temp, temp_im, temp_re
    integer, intent(in) :: n, n_im, n_re
    
    integer :: m_max, m, i
    double precision :: z_im, z_re
    double complex :: z, pole_tmp, res_tmp

    m_max = ceiling(0.5d0 *(mu /(PI*KB*temp_im) - 1.d0))
    
    ! Compute poles and residues for the imaginary term
    do i = 1, n_im
        m = m_max - (i-1)
        z_re = PI * KB * temp_im * (2*m + 1)
        z_im = mu_im
        z = z_re + IM*z_im
        poles_denis(i) = z
        res_denis(i) = fermi(mu, temp, z) - &
                       fermi(mu_re, temp_re, z)
        res_denis(i) = res_denis(i) * (-1.d0*IM*KB*temp_im)
    end do
    
    ! Compute poles and residues for the conventional term
    do i = 1, n
        z = mu + IM*PI*KB*temp*(2*(i-1) + 1)
        poles_denis(n_im + i) = z
        res_denis(n_im + i) = -KB*temp*fermi(IM*mu_im, &
                                             IM*temp_im, z)
    end do

    ! Compute poles and residues for the real term
    do i = 1, n_re
        z = mu_re + IM*PI*KB*temp_re*(2*(i-1) + 1)
        poles_denis(n_im + n + i) = z
        res_denis(n_im+n+i) = KB*temp_re*fermi(IM*mu_im, &
                                               IM*temp_im, z)
    
    end do
  end subroutine denis_poles
    


  function fermi_rel_arg(mu, temp, energy)
    ! A Fermi function on the complex plane 
    implicit none
    double complex, intent(in) :: energy
    double precision, intent(in) :: mu, temp
    double complex :: fermi_rel_arg
    
    fermi_rel_arg = 1.d0 / (1.d0 + &
                            exp((energy - mu)/(KB*temp)))
  end function fermi_rel_arg



  function fermi_im_arg(mu, temp, energy)
    ! A Fermi function on the complex plane
    implicit none
    double complex, intent(in) :: mu, temp, energy
    double complex :: fermi_im_arg
    
    fermi_im_arg = 1.d0 / (1.d0 + &
                           exp((energy - mu)/(KB*temp)))
  end function fermi_im_arg



  function get_temperatures(mu, e_min, temp, p)
  ! Returns a two element array containing
  ! temp_re, and temp_im
    double precision, intent(in) :: mu, e_min, temp
    integer, intent(in) :: p
    double precision, dimension(2) :: get_temperatures
    
    double precision :: temp_re, temp_im
    double precision :: term1, term2

    get_temperatures = 0.d0
    
    term1 = 0.5d0*temp
    term2 = temp + (mu - e_min) / (p*KB)
    temp_im = sqrt(term1 * term2)
    temp_re = temp_im
    
    get_temperatures(1) = temp_im
    get_temperatures(2) = temp_re
  end function get_temperatures
    


  function denis_no_of_poles(mu, mu_im, mu_re, &
                             temp, temp_im, temp_re, p)

    ! Compute the estimated number of poles for the three
    ! contour lines and return them as a 1D array
    ! (n, n_re, n_im)
    
    implicit none
    double precision, intent(in) :: mu, mu_im, mu_re
    double precision, intent(in) :: temp, temp_im, temp_re
    integer, intent(in), optional :: p
    integer :: p_in
    integer :: n_im, n_re, n
    integer, dimension(3) :: denis_no_of_poles 
    double precision :: up, dn

    if (present(p)) then
        p_in = p
    else
        p_in = 21
    endif
    
    ! Number of poles of imaginary kind
    up = mu - mu_re + p_in*KB*temp_re + p_in*KB*temp
    dn = 2*PI*KB*temp_im
    n_im = int(up/dn)
    
    ! Number of poles of conventional kind
    up = 2*mu_im
    dn = 2*PI*KB*temp
    n = int(up/dn)

    ! Number of poles of real kind
    up = 2*mu_im
    dn = 2*PI*KB*temp_re
    n_re = int(up/dn)
    
    denis_no_of_poles(1) = n
    denis_no_of_poles(2) = n_re
    denis_no_of_poles(3) = n_im

  end function denis_no_of_poles
    


  subroutine adjust(temp_im, mu_re, mu)

  ! Adjust the values of temp_im and mu_re in order to
  ! avoid overlap of the poles

    implicit none
    double precision, intent(inout) :: temp_im, mu_re
    double precision, intent(in) :: mu

    integer :: m_max, n_max
    double precision :: temp_im_p, mu_re_p

    ! Adjust the value of temp_im
    m_max = ceiling(0.5d0 * (mu/(PI*KB*temp_im) - 1.d0))
    
    if (m_max .eq. 0) then
        temp_im_p = 2*temp_im 
    else
        temp_im_p = mu / (2*PI*KB*m_max)
    endif
    
    n_max = ceiling(0.5d0 *(mu_re/(PI*KB*temp_im_p)-1d0))
    mu_re_p = 2 * PI * KB * temp_im_p * n_max
    
    temp_im = temp_im_p
    mu_re = mu_re_p
  end subroutine adjust
    


  subroutine init_denis(mu, temp, e_min, p)
  ! Set the adjusted (nonoverlapping poles) and their
  ! corresponding residues
    
  implicit none
  double precision, intent(in) :: mu, temp, e_min
  integer, intent(in), optional :: p
  integer :: p_in

  double precision, dimension(2) :: t_im_re
  integer, dimension(3) :: n_con_re_im
  double precision :: temp_im, temp_re, mu_im, mu_re
  double precision :: delta
  integer :: n, n_im, n_re, ntot
  
  if (present(p)) then
    p_in = p
  else
    p_in = 21
  endif
    
  delta = (mu - e_min) / (KB*temp)
  
  if (delta .gt. 1000.d0) then
    stop "Temperature is too low for Denis integrator"
  end if
    
  t_im_re = get_temperatures(mu, e_min, temp, p_in) 
  temp_im = t_im_re(1)
  temp_re = t_im_re(2)
  
  mu_im = p_in*KB*temp_im
  mu_re = e_min - p_in*KB*temp_re

  call adjust(temp_im, mu_re, mu)

  n_con_re_im = denis_no_of_poles(mu, mu_im, mu_re, &
                                  temp, temp_im, temp_re, &
                                  p_in)
  n = n_con_re_im(1)
  n_re = n_con_re_im(2)
  n_im = n_con_re_im(3)
  
  ntot = n + n_re + n_im

  allocate(poles_denis(ntot))
  allocate(res_denis(ntot))
  
  call denis_poles(mu, mu_im, mu_re, &
                   temp, temp_im, temp_re, &
                   n, n_im, n_re)
  end subroutine init_denis



  function rho_denis(green_func, energy, t, spins, &
                       temperature, jsd)
    implicit none
    real, intent(in) :: energy, t, temperature, jsd
    type(vector3D), dimension(:), intent(in) :: spins
    double complex, dimension(2*size(spins), &
        2*size(spins)) :: temp_denis
    
    interface 
      function green_func(cspins, energy, t, j_sd)
        use math
        type(vector3D), dimension(:), intent(in) :: cspins
        double complex, intent(in) :: energy, t
        double precision, intent(in) :: j_sd

        double complex, dimension(2*size(cspins), &
            2*size(cspins)) :: green_func
      end function 
    end interface

    integer :: ntot, i
    double complex, dimension(2*size(spins), &
                              2*size(spins)) :: rho_denis
    double complex, dimension(2*size(spins), &
                              2*size(spins)) :: sum_denis
    double complex, dimension(2*size(spins), &
                              2*size(spins)) :: gf_temp

    rho_denis = 0.d0
    sum_denis = 0.d0
    
    ntot = size(poles_denis)
    
    do i = 1, ntot
      gf_temp = green_func(spins, poles_denis(i), t=RE*t, &
                           j_sd=1.d0*jsd)
      temp_denis = 2*IM*res_denis(i)*gf_temp
      sum_denis = sum_denis + 0.5*IM*(temp_denis - &
              conjg(transpose(temp_denis)))

    end do
    
    rho_denis = sum_denis
  end function rho_denis
  
  

  function spindensity(rho)
    use math, only: CZERO
    use constants, only: PAULI_X, PAULI_Y, PAULI_Z
    implicit none
    double complex, dimension(:, :), intent(in) :: rho
    double complex, &
        dimension(size(rho, 1), size(rho, 2)) :: sigma
    type(vector3D), dimension(size(rho, 1)/2) :: spindensity
    integer :: n, i
    
    n = size(rho, 1)/2
    sigma = (0.d0, 0.d0) 
    sigma = matmul(rho, (eye(n) .kron. PAULI_X)) 
    do i = 1, n 
      spindensity(i)%x = real(sigma(2*i-1, 2*i-1) + &
                              sigma(2*i, 2*i))
    end do

    sigma = matmul(rho, (eye(n) .kron. PAULI_Y)) 
    do i = 1, n 
      spindensity(i)%y = real(sigma(2*i-1, 2*i-1) + &
                              sigma(2*i, 2*i))
    end do

    sigma = matmul(rho, (eye(n) .kron. PAULI_Z)) 
    do i = 1, n 
      spindensity(i)%z = real(sigma(2*i-1, 2*i-1) + &
                              sigma(2*i, 2*i))
    end do

  end function spindensity



end module ozaki_integrator
