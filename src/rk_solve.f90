SUBROUTINE rk_init

    USE global
    USE rksuite_vars
    implicit none
  integer:: check_alloc, alloc_status
  !NEQ = 9*nn*nn*max(N_Lorentzians_L+N_nu_k_Left+1,N_Lorentzians_R+N_nu_k_right+1)*2    
    NEQ = length_rkvec*2
    LENWRK = 16*NEQ
write(*,*) 'NEQ:',NEQ
    ! *** allocation of missing arrays for rksuite, "missing" since they could not be defined in the MODULE rksuite_vars, because n (and nn) is not a parameter but acquired during the program
  check_alloc = 0
  allocate(WORK(LENWRK), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc= check_alloc+1
    if (check_alloc .NE. 0) write(*,*) 'Error in allocation WORK of RK Variables (rk_init) '
    check_alloc=0
  allocate(THRES(NEQ), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc= check_alloc+1
    if (check_alloc .NE. 0) write(*,*) 'Error in allocation THRES of RK Variables (rk_init) '
    check_alloc=0
  allocate(YMAX(NEQ), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc= check_alloc+1
    if (check_alloc .NE. 0) write(*,*) 'Error in allocation YMAX of RK Variables (rk_init) '
  if (check_alloc .EQ. 0) write(*,*) 'rk allocation ok'

  TSTART = t_0
  TEND   = t_end*1.5d0
  THRES=TOL
  CALL ENVIRN(OUTCH,MCHPES,DWARF)
  CALL SETUP(NEQ,TSTART,rkvec,TEND,TOL,THRES,METHOD,TASK,ERRASS,TSTART,WORK,LENWRK,MESSAGE)

end subroutine rk_init


SUBROUTINE solve_with_rk(TWANT)
    USE global
    USE rksuite_vars
    implicit none
    double precision:: TWANT
    external eom
!write(*,*)'solve_with_rk called'
    call UT(eom,TWANT,TNOW,rkvec,drkvec,YMAX,WORK,ifail)
!write(*,*)'UT check'
    if (ifail > 4) write(*,*) "rk error: ", ifail
!write(*,*) 'ifail:', ifail, TNOW
!   *** on soft errors:
!   *** repeat until TWANT is reached (TNOW=TWANT)
    DO WHILE ( (IFAIL.eq.2 .or. IFAIL.eq.3 .or. IFAIL.eq.4).and. TNOW .ne. TWANT )
        IFAIL = -1
        call UT(eom,TWANT,TNOW,rkvec,drkvec,YMAX,WORK,ifail)
        if (ifail > 4) write(*,*) "rk error: ", ifail
    END DO
    
END SUBROUTINE solve_with_rk 
