module green_1d

  use math
  use constants

contains


  function green(cspins, energy, t, j_sd)
    use global, only: H
    implicit none
    type(vector3D), dimension(:), intent(in) :: cspins
    double complex, intent(in) :: energy, t
    double precision, intent(in) :: j_sd
    double complex, dimension(2*size(cspins), 2*size(cspins)) :: green, ham
    double complex :: se
    integer :: n, i, j
    
    n = size(cspins)
    ham = H
    se = selfenergy(energy) 
    
    ham(1:2, 1:2) = ham(1:2, 1:2) + (se * eye(2)) 
    ham(2*n-1:2*n, 2*n-1:2*n) = ham(2*n-1:2*n, 2*n-1:2*n) + (se * eye(2))

    green = inverse((energy*eye(2*n)) - ham)
    
  end function green
    
  
  function selfenergy(energy, t, t_ls)
    implicit none
    double complex, intent(in) :: energy
    double complex, intent(in), optional :: t, t_ls
    double complex :: selfenergy
    double complex :: t_connect, t_lead, rad
    double precision :: sgn
    
    t_connect = (1.0d0, 0.0d0)
    t_lead  = (1.0d0, 0.0d0)
    
    if (present(t)) t_lead = t
    if (present(t_ls)) t_connect = t_ls

    rad = 4*t_lead**2 - energy**2
    selfenergy = (0.0d0, 0.0d0) 
    if (real(rad) > 0) then
      selfenergy = energy - IM * sqrt(rad)
    else
      if (real(energy) > 0) then
        sgn = 1.0
      else
        sgn = -1.0
      end if
      selfenergy = energy - sgn * sqrt(-rad)
    end if
    selfenergy = selfenergy * (t_connect**2 / (2*t_lead**2))
  end function selfenergy


end module green_1d
