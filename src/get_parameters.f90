subroutine get_parameters(filename, llg_params, gopt)

  use global
  use llg, only: llg_parameters, read_llg_params
  implicit none
  integer :: i, j, k, cel
  integer :: io_error, alloc_status,check,check_alloc
  character (len=4) ::  sqstring 
  character (len=*), intent(in) :: filename
  type(llg_parameters), intent(inout) :: llg_params
  type(global_options) :: gopt
  real :: vbias, fermi_energy
        
  check = 0
  check_alloc = 0
    
  open(unit=11, file=filename, status='old', action='read', &
       iostat=io_error)

  read(11,*,iostat=io_error)  n                ! Number of sites
  if (io_error .ne. 0) check = check + 1

  read(11,*,iostat=io_error)  n_zero           ! DW position at the start 
  if (n_zero .gt. n) then 
    stop "The domain wall must be placed inside the system (1 < n_zero < n)"
  end if
  if (io_error .ne. 0) check = check + 1
 
  read(11,*,iostat=io_error) n_lorentz         ! Number of lorentzians 
  if (io_error .ne. 0) check=check+1
    
  READ(11,*,iostat=io_error) n_channels        ! Number of channels 
  if (io_error .ne. 0) check=check+1            ! For spinfull case set to 2

  ! Energy shift of the bands  
  READ(11,*,iostat=io_error) delta_tdep_L, delta_tdep_R   
  if (io_error .ne. 0) check=check+1 

  ! Fermi energies of the leads
!  READ(11,*,iostat=io_error) E_F_left, E_F_right  
!  if (io_error .ne. 0) check=check+1
   READ(11,*,iostat=io_error) E_F_system, V_bias 
  if (io_error .ne. 0) check=check+1
   E_F_left = E_F_system 
   E_F_right = E_F_system

   !Rashba parameter
  READ(11,*,iostat=io_error) alpha 
  if (io_error .ne. 0) check=check+1
   


   !Angles first sublattice
  READ(11,*,iostat=io_error) theta_1, phi_1 
  if (io_error .ne. 0) check=check+1
   
   !Angles second sublattice
  READ(11,*,iostat=io_error) theta_2, phi_2 
  if (io_error .ne. 0) check=check+1
  



   !Period
  READ(11,*,iostat=io_error) period 
  if (io_error .ne. 0) check=check+1
   

   !N_rash # number of spin that start with rashba 
  READ(11,*,iostat=io_error) N_rash 
  if (io_error .ne. 0) check=check+1
   









 

  ! Temperature, number of poles in the Fermi function expansion
  READ(11,*,iostat=io_error) Temp, N_poles   
  if (io_error .ne. 0) check=check+1

  ! Initial time, end time, t_step of output      
  READ(11,*,iostat=io_error) t_0, t_step, t_end 
  if (io_error .ne. 0) check=check+1
  read(11,*,iostat=io_error)  light_flag          
  if (io_error .ne. 0) check = check + 1
  READ(11,*,iostat=io_error) J_sd             ! Coupling term to LLG part 
  if (io_error .ne. 0) check=check+1
  READ(11,*,iostat=io_error) n_precessing   ! Num. of precessing spins

  if (io_error .ne. 0) check=check+1
  READ(11,*,iostat=io_error) dw_width 

  if (io_error .ne. 0) check=check+1 
  read(11, *, iostat=io_error) t_long
  if (io_error .ne. 0) check = check + 1 
  read(11, *, iostat=io_error) t_bias         ! Time at which to turn on bias
  if (io_error .ne. 0) check = check + 1 
  read(11, *, iostat=io_error) llg_step
  if (io_error .ne. 0) check = check + 1 

  if (check .ne. 0) write(*,*) 'Error reading parameters file: '//filename  

  check = 0
  check_alloc = 0

  ! Read LLG parameters
  llg_params = read_llg_params(11)
  llg_params % n_sites = n 
  gopt = read_global_options(11)
  
  close(unit=11)
    
  DO i=1,2
    DO j=1,2
      IF (i==j) THEN 
        I_2(i,j) = dcmplx(1.d0,0.d0)
      ELSE
        I_2(i,j) = dcmplx(0.d0,0.d0)
      END IF
    END DO
  END DO
    
  allocate(I_n(n,n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1

  DO i=1,n
    DO j=1,n
      IF (i==j) THEN 
        I_n(i,j) = dcmplx(1.d0,0.d0)
      ELSE
        I_n(i,j) = dcmplx(0.d0,0.d0)
      END IF
    END DO
  END DO
    
  sigma_x(1,1) = dcmplx(0.d0,0.d0)
  sigma_x(1,2) = dcmplx(1.d0,0.d0)
  sigma_x(2,1) = dcmplx(1.d0,0.d0)
  sigma_x(2,2) = dcmplx(0.d0,0.d0)
    
  sigma_y(1,1) = dcmplx(0.d0,0.d0)
  sigma_y(1,2) = dcmplx(0.d0,-1.d0)
  sigma_y(2,1) = dcmplx(0.d0,1.d0)
  sigma_y(2,2) = dcmplx(0.d0,0.d0)
    
  sigma_z(1,1) = dcmplx(1.d0,0.d0)
  sigma_z(1,2) = dcmplx(0.d0,0.d0)
  sigma_z(2,1) = dcmplx(0.d0,0.d0)
  sigma_z(2,2) = dcmplx(-1.d0,0.d0)
    
    
  theta_spin = pi / 4.d0  
    
  omega_spin = 0.1d0 * (1.d0 / hbar) 
    
  J_spin     = 1.d0 
    
    
  allocate(E(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
    
  do j=1,n
    E(j)= 0.0d0
  end do
    
  allocate(delta(n-1), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1

  do j=1,n-1
    delta(j)= 1.d0
  end do

  allocate(Sx_classical(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Sy_classical(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Sz_classical(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1

  ! steady state spin densit read from file
  allocate(spindensity_x_stdstat(n), stat= alloc_status)  
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(spindensity_y_stdstat(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1 
  allocate(spindensity_z_stdstat(n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1 

  allocate(H_spinless(n,n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1 
  allocate(H(2*n,2*n), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(rho(2*n,2*n), stat= alloc_status)            
  if (alloc_status .ne.0) check_alloc=check_alloc+1        
  allocate(rho_ozaki(2*n,2*n), stat= alloc_status)            
  if (alloc_status .ne.0) check_alloc=check_alloc+1        
    
  if(check_alloc .ne.0) write(*,*) 'Error in allocation of H/rho/G!'  
  check_alloc = 0

  k_poles = N_poles + n_lorentz        
 
  allocate(Pi_L(2*n,2*n), stat= alloc_status)            
  if (alloc_status .ne.0) check_alloc=check_alloc+1 
  allocate(Pi_R(2*n,2*n), stat= alloc_status)            
  if (alloc_status .ne.0) check_alloc=check_alloc+1 
        
  allocate(gam_L(n_lorentz,n_channels), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(eps_L(n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(w0_L(n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
        
        
  allocate(gam_R(n_lorentz,n_channels), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(eps_R(n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(w0_R(n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
 
     
  write (sqstring,'(I0)') n_lorentz
  open(unit=12, file=gopt % selfenergy_file_path//&
       &'selfenergy/selfenergy_1DTB_NNLS_'&
       &//trim(sqstring)//'_pbest.csv', status='old',action='read', &
       iostat=io_error)
!  open(unit=12, file='./res/selfenergy/selfenergy-1Dwire-pbest-NL'&
!       &//trim(sqstring)//'.csv', status='old',action='read', &
!       iostat=io_error)
  DO i=1,n_lorentz
    READ(12,*,iostat=io_error) eps_L(i) 
    READ(12,*,iostat=io_error) w0_L(i)
    if (io_error .ne.0) check=check+1
           
    eps_R(i) =  eps_L(i) 
    w0_L(i)  =  abs(w0_L(i))
    w0_R(i)  =  w0_L(i)       
  END DO
  CLOSE(unit=12)
    
!  write(*,*) 'eps=', eps_L(:)
!  write(*,*) 'w0=' , w0_L(:)

  open(unit=13, file=gopt % selfenergy_file_path//&
       &'selfenergy/selfenergy_1DTB_NNLS_'&
       &//trim(sqstring)//'_Ulsq.csv', status='old',action='read', &
       iostat=io_error)
!   OPEN(unit=13, file='./res/selfenergy/selfenergy-1Dwire-Ulsq-NL'//trim(sqstring)//'.csv', status='old',action='read', iostat=io_error)

  DO k=1,n_lorentz
    READ(13,*,iostat=io_error) (gam_L(k,i),i=1,1)
    if (io_error .ne.0) check=check+1
  END DO
  CLOSE(unit=13)   
    
        
  DO k=1,n_lorentz
!   DO i=1,n_channels
    gam_L(k,2) = gam_L(k,1)
    gam_R(k,1) = gam_L(k,1)
    gam_R(k,2) = gam_L(k,1)
            
!   END DO
  END DO 
        
!  write(*,*)
!  write(*,*) 'gam_L'
!  DO k=1,n_lorentz
!    write(*,*) ( gam_L(k,i),i=1,n_channels )             
!  END DO 
            
  allocate(hi_left_m(k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(hi_left_p(k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(hi_right_m(k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(hi_right_p(k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1  

  if(check_alloc .ne.0) write(*,*) 'Error in allocation of hi !'
    
  check_alloc = 0
 
  allocate(Gam_greater_L_m(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_greater_L_p(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_lesser_L_m(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_lesser_L_p(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1

  allocate(Gam_greater_R_m(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_greater_R_p(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_lesser_R_m(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(Gam_lesser_R_p(n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1

  if(check_alloc .ne.0) write(*,*) 'Error in allocation of Gam !' 
  check_alloc = 0
        
  allocate(psi_L(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(psi_R(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
        
  allocate(summ1_L(2*n,n_channels,n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(summ2_L(2*n,n_channels,n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(summ3_L(2*n,n_channels,k_poles-n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
    
  allocate(summ1_R(2*n,n_channels,n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(summ2_R(2*n,n_channels,n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(summ3_R(2*n,n_channels,k_poles-n_lorentz), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
        
  allocate(csi_L(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(csi_R(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
    
  allocate(csi_loop(2*n,1,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(psi_loop(2*n,1,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1


  allocate(Omega_LL1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_LL2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_LL3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_LR1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_LR2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_LR3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
    
    
  allocate(Omega_RR1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_RR2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_RR3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_RL1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_RL2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(Omega_RL3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
    
  allocate(drho(2*n,2*n), stat= alloc_status)            
  if (alloc_status .ne. 0) check_alloc=check_alloc+1 

  allocate(dpsi_L(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dpsi_R(2*n,n_channels,k_poles), stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
    
  allocate(dOmega_LL1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_LL2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_LL3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_LR1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_LR2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_LR3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
    
  allocate(dOmega_RR1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_RR2(n_channels,k_poles-n_lorentz,n_channels,n_lorentz), &
           stat=alloc_status)      
  if (alloc_status .ne. 0) check_alloc=check_alloc+1
  allocate(dOmega_RR3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(dOmega_RL1(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(dOmega_RL2(n_channels,n_lorentz,n_channels,n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(dOmega_RL3(n_channels,n_lorentz,n_channels,k_poles-n_lorentz), &
           stat= alloc_status)      
  if (alloc_status .ne.0) check_alloc=check_alloc+1
    
    
  if(check_alloc .ne.0) write(*,*) 'Error in allocation of Pi or Omega!'

  length_rkvec = 2*n * 2*n + 2*n*n_channels*(k_poles+k_poles) + &
                 2*n_channels*n_channels * &
                   (n_lorentz*n_lorentz + n_lorentz*(k_poles-n_lorentz) + &
                   (k_poles-n_lorentz)*n_lorentz )  &
                + 2*n_channels*n_channels * &
                   (n_lorentz*n_lorentz + n_lorentz*(k_poles-n_lorentz) + &
                   (k_poles-n_lorentz)*n_lorentz)

  allocate(rkvec(length_rkvec), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
  allocate(drkvec(length_rkvec), stat= alloc_status)
  if (alloc_status .ne.0) check_alloc=check_alloc+1
    
  write(*,*) 'allocation size of rkvec/ drkvec', size(rkvec),size(drkvec)

  if (check_alloc .ne. 0) write(*,*) 'Error in allocation of rkvec/drkvec!'

  check_alloc = 0
    
  beta = 1.d0 / (k_Boltzmann*Temp)

  ! Allocation of matrices used to compute the 
  ! Ozaki-poles of the Fermi function

  allocate(Eig_val_mat_L(2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
            
  allocate(Mat_L(2*N_poles,2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(Nu_L(N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(R_alpha_L(2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(R_L(N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'

    
  allocate(Eig_val_mat_R(2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
       
  allocate(Mat_R(2*N_poles,2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(Nu_R(N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(R_alpha_R(2*N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'
  allocate(R_R(N_poles),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'

  length_workspace = 3*(2*N_poles)-1
       
  allocate(workspace_for_diag(length_workspace),stat=alloc_status)
  if(alloc_status .ne. 0) write(*,*) 'Error in allocation'

end subroutine get_parameters 
