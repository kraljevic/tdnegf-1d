subroutine kron(n, m, nm, a, b, c)

implicit none

      integer ::  n, m, nm
      integer ::  i, j, k, l
       
      double complex, dimension(n,n) :: a 
      double complex, dimension(m,m) :: b 
      double complex, dimension(nm,nm) :: c

      do i=1,n
        do j=1,n
          do k=1,m
            do l=1,m

              c(k+m*(i-1),l+m*(j-1))=a(i,j)*b(k,l)

            end do
          end do
        end do
      end do


END SUBROUTINE kron


