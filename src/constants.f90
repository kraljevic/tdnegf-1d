module constants

 use math, only: IM, RE, CZERO


 double precision, parameter :: MBOHR = 5.788381e-05
 double precision, parameter :: KB = 8.6173324d-5
 double precision, parameter :: GAMMA_R = 1.760859644e-4

 double complex, dimension(2, 2), parameter :: &
   PAULI_X = RE * reshape((/ 0.0, 1.0, 1.0, 0.0/), (/2, 2/))

! The following definition might look wrong, but remember that
! reshape in Fortran works column-wise and not row-wise (as in e.g. numpy)
 double complex, dimension(2, 2), parameter :: &
   PAULI_Y = reshape((/ CZERO, IM, -IM, CZERO/), (/2, 2/))

 double complex, dimension(2, 2), parameter :: &
   PAULI_Z = reshape((/ RE, CZERO, CZERO, -RE/), (/2, 2/))

end module constants
