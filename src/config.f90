module config

  use global
  use llg, only: llg_parameters
  use precess, only: prec_spin
  use math, only: operator (-)

contains

  subroutine configure(cname, lp, pr_spins)

  ! NOTE: Classical spins (Si_classical) as well as 
  ! local Jsd parameters that go into the NEGF Hamiltonian
  ! (jsd_llg_to_tdnegf), and number of sites 'n' and 'n_zero',
  ! are global variables (see global_variables.f90)
  ! so they are not passed to this function explicitly
  ! although they are used and modified in it.

  implicit none

  character (len=*), intent(in) :: cname  ! Configuration
  type(prec_spin), allocatable, &
      dimension(:), intent(inout) :: pr_spins ! Precessing S 
  type(llg_parameters), intent(inout) :: lp

  ! Auxiliary variables
  double precision :: theta, af_sign, coefs, theta_r, phi_r
  integer :: i, j, jj, delta_spacer
  double precision :: theta_t, phi_t
  real :: rand(n)


  if (cname .eq. 'ohe') then
    do jj = 1, n
      Sx_classical(jj) = 1.0d0 / cosh(1.0*(n_zero - jj))
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = -1.0d0 * tanh(1.0*(n_zero - jj))
    end do

  else if (cname .eq. 'annihilate') then

    do jj = 1, n
      theta = acos(tanh(1.0*(jj - n_zero))) + &
              acos(tanh(1.0*(jj - (n-n_zero))))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'annihilate2') then

    do jj = 1, n
      theta = acos(tanh((1.0/sqrt(2.0))*(jj - n_zero))) + &
              acos(tanh((1.0/sqrt(2.0))*(jj - (n-n_zero))))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'annihilate2_edge') then

    do jj = 1, n
      theta = acos(tanh((1.0/sqrt(2.0))*(jj - n_zero))) + &
              acos(tanh((1.0/sqrt(2.0))*(jj - (n-n_zero))))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do
    do jj = 2, n-1
      jsd_llg_to_tdnegf(jj) = 0.0
      lp%js_sd(jj) = 0.0
    end do

  else if (cname .eq. 'annihilate_reverse') then

    do jj = 1, n
      theta = acos(tanh(-1.0*(jj - n_zero))) + &
              acos(tanh(+1.0*(jj - (n-n_zero))))
      theta = theta + 3.141592653589793
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'annihilate_reverse_edge') then

    do jj = 1, n
      theta = acos(tanh(-1.0*(jj - n_zero))) + &
              acos(tanh(+1.0*(jj - (n-n_zero))))
      theta = theta + 3.141592653589793
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

    do jj = 2, n-1
      jsd_llg_to_tdnegf(jj) = 0.0
      lp%js_sd(jj) = 0.0
    end do

  else if (cname .eq. 'annihilate2_reverse') then

    do jj = 1, n
      theta = acos(tanh((-1.0/sqrt(2.0))*(jj - n_zero))) + &
              acos(tanh((+1.0/sqrt(2.0))*(jj - (n-n_zero))))
      theta = theta + 3.141592653589793
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'ohe_afm') then

    do jj = 1, n
      if (mod(jj, 2) .eq. 1) then
        af_sign = -1.0d0
      else
        af_sign = +1.0d0
      endif
      Sx_classical(jj) = af_sign/cosh((n_zero - jj)/dw_width)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = -af_sign*tanh((n_zero - jj)/dw_width)
    end do
  else if (cname .eq. 'var_fm') then
    ! Very similar to the next 'p_fm' configuration
    ! except the n_zero determines where the polarizer
    ! ends and there is a gradual reduction of polarizer
    ! Jsd to main system Jsd
    if (n .le. 9) then
        stop "Error! Number of sites must be greater than 9"
    end if
    

    theta_r = lp%p_theta * 3.141592 / 180.
    phi_r = lp%p_phi * 3.141592 / 180.

    do jj = 1, n_zero 
      Sx_classical(jj) = sin(theta_r)*cos(phi_r) 
      Sy_classical(jj) = sin(theta_r)*sin(phi_r)
      Sz_classical(jj) = cos(theta_r)
    end do

    do jj = n_zero + 1, n
      Sx_classical(jj) = 0.0d0
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = 1.0d0
    end do

    ! Prevent dynamics on the polarizer
    do jj = 1, n_zero 
      lp%js_sd(jj) = 0.0
      lp%js_exc(jj) = 0.d0
    end do
    
    ! Set specific coupling for the polarizer
    do jj = 1, 3
      jsd_llg_to_tdnegf(jj) = lp%js_pol
    end do

    ! Add decreasing coupling in the rest
    coefs = lp%js_pol
    delta_spacer = n_zero - 3
    do jj = 4, n_zero 
      coefs = lp%js_pol*(delta_spacer - jj + 3)/delta_spacer
      jsd_llg_to_tdnegf(jj) = coefs
    end do
    
    ! Kill anisotropy and demagnetization on the polariser
    do jj = 1, n_zero 
        lp%js_ani(jj) = 0.d0
        lp%js_dem(jj) = 0.d0
    end do

  else if (cname .eq. 'p_afm' .or. &
           cname .eq. 'p_afm_ohe' .or. &
           cname .eq. 'p_fm') then
    ! Configurations in which there is a polarizer on 
    ! the first three sites (with special Jsd_pol).
    ! Then, there are two moments without any coupling  
    ! (a spacer) and a lattice of normal moments
    ! (AFM along z, FM along z, or Ohe DW). On the other end,
    ! there is an analyzer (with Jsd_anl) 
    ! The system requires at least 3+2+2+3 = 10 moments.

    if (n .le. 9) then
        stop "Error! Number of sites must be greater than 9"
    end if

    ! Create a x-polarizer on the first three sites
    ! and add two sites for the separator

    theta_r = lp%p_theta * 3.141592d0 / 180.d0
    phi_r = lp%p_phi * 3.141592d0 / 180.d0
    print *, "THETA",  theta_r, lp%p_theta, n

    do jj = 1, 5
      Sx_classical(jj) = sin(theta_r)*cos(phi_r) 
      Sy_classical(jj) = sin(theta_r)*sin(phi_r)
      Sz_classical(jj) = cos(theta_r)
    end do

    ! Create FM and AFM configurations
    do jj = 6, n-3
      if (mod(jj, 2) .eq. 0) then
        af_sign = 1.0d0
      else
        af_sign = -1.0d0
      endif
      if (cname .eq. 'p_afm') then
        Sx_classical(jj) = 0.0d0
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = af_sign
      else if (cname .eq. 'p_fm') then
        Sx_classical(jj) = 0.0d0
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = 1.0d0
      else if (cname .eq. 'p_afm_ohe') then
        Sx_classical(jj) = af_sign/cosh((n_zero - &
                                         jj)/dw_width)
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = -af_sign*tanh((n_zero - &
                                          jj)/dw_width)
      endif
    end do

    ! Create analyzer
    do jj = n-2, n
      Sx_classical(jj) = 1.0d0
      Sy_classical(jj) = 0.0d0
      Sy_classical(jj) = 0.0d0
    end do

    ! Prevent dynamics of the first five sites
    ! (polariser and separator)
    do jj = 1, 5
      lp%js_sd(jj) = 0.0
      lp%js_exc(jj) = 0.d0
    end do
    
    ! Set specific coupling for the polarizer
    do jj = 1, 3
      jsd_llg_to_tdnegf(jj) = lp%js_pol
    end do

    ! Prevent two separator sites to influence electrons
    do jj = 4, 5
      jsd_llg_to_tdnegf(jj) = 0.0
    end do

    ! Prevent sites in the analyser to influence electrons 
    ! And set specific hopping in the analyzer
    do jj = n-2, n
      jsd_llg_to_tdnegf(jj) = 0.0
      lp%js_sd(jj) = lp%js_ana
    end do

    ! Kill exchange interaction after the main region
    do jj = n-3, n-1
        lp%js_exc(jj) = 0.d0
    end do

    ! Set hopping in the central part
    do jj = 6, n-4
        delta(jj) = lp%thop
    end do
    
    ! Kill anisotropy and demagnetization on the polariser
    do jj = 1, 5
        lp%js_ani(jj) = 0.d0
        lp%js_dem(jj) = 0.d0
    end do

    ! Kill anisotropy and demagnetization on the analyzer
    do jj = n-2, n 
        lp%js_ani(jj) = 0.d0
        lp%js_dem(jj) = 0.d0
    end do

  else if (cname .eq. 'annihilate2_reverse_edge') then

    do jj = 1, n
      theta = acos(tanh((-1.0/sqrt(2.0))*(jj - n_zero))) + &
              acos(tanh((+1.0/sqrt(2.0))*(jj - (n-n_zero))))
      theta = theta + 3.141592653589793
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do
    do jj = 2, n-1
      jsd_llg_to_tdnegf(jj) = 0.0
      lp%js_sd(jj) = 0.0
    end do

  else if (cname .eq. 'annihilate_reverse_conversion') then
    do jj = 1, n
      theta = acos(tanh(-1.0*(jj - n_zero))) + &
              acos(tanh(+1.0*(jj - (n-n_zero))))
      theta = theta + 3.141592653589793
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

    ! 1---2---3---4 Site
    ! O---O---O---O System
    ! --1---2---3-- Hopping
    ! The point is to cancel Jsd interaction on the n-1 site
    ! and last hopping
    jsd_llg_to_tdnegf(n-1) = 0.0
    lp%js_sd(n-1) = 0.0
    delta(n-1) = 0.0

  else if (cname .eq. 'repel') then

    do jj = 1, n
      theta = acos(tanh(1.0*(jj - n_zero))) - &
              acos(tanh(1.0*(jj - (n-n_zero))))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'topological') then

    do jj = 1, n
      theta = acos(tanh(1.0*(jj - n_zero)))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'topo_sw') then

    do jj = 1, n
      theta = acos(tanh(1.0*(jj - n_zero)))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do
    allocate(pr_spins(1))
    pr_spins(1)%i = 1 
    pr_spins(1)%theta_zero = 5 
    pr_spins(1)%axis_theta = 90
    pr_spins(1)%axis_phi = 0
    pr_spins(1)%T = 20. 

  else if (cname .eq. 'topological_reverse') then

    do jj = 1, n
      theta = acos(tanh(-1.0*(jj - n_zero)))
      Sx_classical(jj) = cos(theta)
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = sin(theta)
    end do

  else if (cname .eq. 'ohe_flip') then
    do jj = 1, n
      Sx_classical(jj) = 1.0d0 / cosh(1.0*(n_zero - jj))
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = 1.0d0 * tanh(1.0*(n_zero - jj))
    end do

  else if (cname .eq. 'dw2') then

    do jj = 1, n
      if (jj < n/2) then
        Sx_classical(jj) = 1.0 / cosh(1.0*(n_zero - jj))
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = -1.0d0 * tanh(1.0*(n_zero - jj))
      else
        Sx_classical(jj) = 1.0 / cosh(1.0*((n-n_zero) - jj))
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = 1.0d0 * tanh(1.0*((n-n_zero) - jj))
      end if
    end do

  else if (cname .eq. 'along_x') then

    do jj = 1, n
      Sx_classical(jj) = 1.0d0
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = 0.0d0
    end do

  else if (cname .eq. 'along_y') then

    do jj = 1, n
      Sx_classical(jj) = 0.0d0
      Sy_classical(jj) = 1.0d0
      Sz_classical(jj) = 0.0d0
    end do

  else if (cname .eq. 'along_z') then

    do jj = 1, n
      Sx_classical(jj) = 0.0d0
      Sy_classical(jj) = 0.0d0
      Sz_classical(jj) = 1.0d0
    end do

    ! allocate(pr_spins(1))
    ! pr_spins(1)%i = 5 
    ! pr_spins(1)%theta_zero = 90
    ! pr_spins(1)%axis_theta = 90
    ! pr_spins(1)%axis_phi = 90
    ! pr_spins(1)%T = 20.
!-----------------------------------------
! config without analyser
!-----------------------------------------

  else if (cname .eq. 'p_afm_1' .or. &
           cname .eq. 'p_fm_1') then
    ! Configurations in which there is a polarizer on 
    ! the first three sites (with special Jsd_pol).
    ! Then, there are two moments without any coupling  
    ! (a spacer) and a lattice of normal moments
    ! (AFM along z, FM along z, or Ohe DW). On the other end,
    ! there is an analyzer (with Jsd_anl) 
    ! The system requires at least 3+2+2+3 = 10 moments.

    if (n .le. 9) then
        stop "Error! Number of sites must be greater than 9"
    end if

    ! Create a x-polarizer on the first three sites
    ! and add two sites for the separator

    theta_r = lp%p_theta * pi / 180.d0
    phi_r = lp%p_phi * pi / 180.d0
    print *, "THETA",  theta_r, lp%p_theta
    print *, "PHI",  phi_r, lp%p_phi

    do jj = 1, 5
      Sx_classical(jj) = sin(theta_r)*cos(phi_r) 
      Sy_classical(jj) = sin(theta_r)*sin(phi_r)
      Sz_classical(jj) = cos(theta_r)
    end do

    ! Create FM and AFM configurations
    do jj = 6, n
      if (mod(jj, 2) .eq. 0) then
        af_sign = 1.0d0
      else
        af_sign = -1.0d0
      endif
      if (cname .eq. 'p_afm_1') then
        Sx_classical(jj) = 0.0d0
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = af_sign
      else if (cname .eq. 'p_fm_1') then
        Sx_classical(jj) = 0.0d0
        Sy_classical(jj) = 0.0d0
        Sz_classical(jj) = 1.0d0
      endif
    end do


    ! Prevent dynamics of the first five sites
    ! (polariser and separator)
    do jj = 1, 5
      lp%js_sd(jj) = 0.0
      lp%js_exc(jj) = 0.d0
    end do
    
    ! Set specific coupling for the polarizer
    do jj = 1, 3
      jsd_llg_to_tdnegf(jj) = lp%js_pol
    end do

    ! Prevent two separator sites to influence electrons
    do jj = 4, 5
      jsd_llg_to_tdnegf(jj) = 0.0
    end do



    ! Set hopping in the central part
    do jj = 6, n
        delta(jj) = lp%thop
    end do
    
    ! Kill anisotropy and demagnetization on the polariser
    do jj = 1, 5
        lp%js_ani(jj) = 0.d0
        lp%js_dem(jj) = 0.d0
    end do
!-----------------------------------------
! just afm
!-----------------------------------------

  else if (cname .eq. 'afm_z') then
    !print *, random_number(r)
    call random_seed()
    call random_number(rand)
    !print *,'random #', r(1)
    ! Create FM and AFM configurations
    do jj = 1, n
      theta_t = 0
      phi_t = 2*pi*rand(jj)
      if (mod(jj, 2) .eq. 0) then
        theta_t = pi + theta_t
      else
        theta_t = theta_t
      endif
      if (cname .eq. 'afm_z') then
        Sx_classical(jj) = sin(theta_t)*cos(phi_t)
        Sy_classical(jj) = sin(theta_t)*sin(phi_t)
        Sz_classical(jj) = cos(theta_t)
      endif
    end do
!-----------------------------------------
! afi
!-----------------------------------------

  else if (cname .eq. 'afi') then
    ! Create FM and AFM configurations
    do jj = 1, n
      theta_t = 0
      if (mod(jj, 2) .eq. 0) then
        theta_t = pi + theta_t
      else
        theta_t = theta_t
      endif
      if (cname .eq. 'afi') then
        Sx_classical(jj) = sin(theta_t)
        Sy_classical(jj) = sin(theta_t)
        Sz_classical(jj) = cos(theta_t)
      endif
    end do 
!-----------------------------------------
! afi pump
!-----------------------------------------

 else if (cname .eq. 'afi_pump') then
    do jj = 1, n_precessing ! 4
      !theta_t = 0
      if (mod(jj, 2) .eq. 0) then
        theta_t = pi + theta_1
      else
        theta_t = theta_2
      endif
      if (cname .eq. 'afi_pump') then
        Sx_classical(jj) = sin(theta_t)
        Sy_classical(jj) = sin(theta_t)
        Sz_classical(jj) = cos(theta_t)
      endif
    end do

   do jj= n_precessing+1, n
    Sx_classical(jj)=0.0
    Sy_classical(jj)=0.0
    Sz_classical(jj)=1.0
    jsd_llg_to_tdnegf(jj)=0.0
  end do
    
    ! Create FM and AFM configurations
    allocate(pr_spins(n_precessing))
    do jj=1,n_precessing ! 4
    ! jj = 1
         pr_spins(jj)%i=jj
         pr_spins(jj)%T=period
       if (mod(jj,2).eq. 0) then
        ! pr_spins(jj)%i = jj 
         pr_spins(jj)%theta_zero = theta_1
         pr_spins(jj)%axis_theta = 0
         pr_spins(jj)%axis_phi = 0
        ! pr_spins(jj)%T = 10
       else!jj =2
    ! pr_spins(jj)%i = jj 
         pr_spins(jj)%theta_zero = theta_2 + 180
         pr_spins(jj)%axis_theta = 0
         pr_spins(jj)%axis_phi = 0
        ! pr_spins(jj)%T = 10
       endif
     end do


!-----------------------------------------
! ferro pump
!-----------------------------------------
 else if (cname .eq. 'ferro_pump') then
    do jj = 1, n_precessing ! 4
      !theta_t = 0
      
      theta_t = theta_1

      if (cname .eq. 'ferro_pump') then
        Sx_classical(jj) = sin(theta_t)
        Sy_classical(jj) = sin(theta_t)
        Sz_classical(jj) = cos(theta_t)
      endif
    end do

   do jj= n_precessing+1, n
    Sx_classical(jj)=0.0
    Sy_classical(jj)=0.0
    Sz_classical(jj)=1.0
    jsd_llg_to_tdnegf(jj)=0.0
  end do
    
    ! Create FM and AFM configurations
    allocate(pr_spins(n_precessing))
    do jj=1,n_precessing ! 4
    ! jj = 1
      pr_spins(jj)%i=jj
      pr_spins(jj)%T=period
        ! if (mod(jj,2).eq. 0) then
        ! pr_spins(jj)%i = jj 
      pr_spins(jj)%theta_zero = theta_1
      pr_spins(jj)%axis_theta = 0
      pr_spins(jj)%axis_phi = 0
        ! pr_spins(jj)%T = 10
     !  else!jj =2
    ! pr_spins(jj)%i = jj 
    !     pr_spins(jj)%theta_zero = theta_2 + 180
    !     pr_spins(jj)%axis_theta = 0
    !     pr_spins(jj)%axis_phi = 0
        ! pr_spins(jj)%T = 10
    !   endif

     end do





!-----------------------------------------
! end
!-----------------------------------------
 
  else
    stop "Error! Unknow option for cspin orientation"
  end if
    
  end subroutine configure


end module config
