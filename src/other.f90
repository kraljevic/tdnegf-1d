DOUBLE COMPLEX FUNCTION fermi(energy)
 	USE global, only: beta
	implicit none
	double complex:: energy

	fermi=dcmplx(1.d0,0.d0) / ( dcmplx(1.d0,0.d0) + cdexp( beta*energy ) ) 

END FUNCTION fermi


DOUBLE COMPLEX FUNCTION spectraldensity(en,epsil,wid)
  
	implicit none
	double complex:: en, epsil, wid

	spectraldensity = wid**2 / ( (en - epsil)**2 + wid**2 )
 
	 
END FUNCTION spectraldensity




DOUBLE COMPLEX FUNCTION cdtrace(A,B)
  implicit none
  integer, intent(IN)          :: B
  double complex, intent(IN)   :: A(B,B)
  integer                      :: i

  cdtrace = dcmplx(0.d0,0.d0)
  do i=1,B
     cdtrace = cdtrace + A(i,i)
  end do
  return
END FUNCTION cdtrace 
