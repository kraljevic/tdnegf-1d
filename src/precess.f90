module precess
  
  use llg, only: vector3D

  type  :: prec_spin
    integer :: i = 1
    double precision :: axis_phi = 0.d0
    double precision :: axis_theta = 0.d0
    double precision :: phi_zero = 0.d0
    double precision :: theta_zero = 0.d0
    double precision :: start_time = 0.d0
    double precision :: T = 10.d0
    type(vector3D) :: s = vector3D(0., 0., 1.) 
    contains
        procedure, pass(this) :: update
  end type prec_spin

contains
  
  subroutine update(this, time)
    class(prec_spin) :: this
    double precision :: time
    double precision :: sx, sy, sz, sxp, syp, szp
    double precision :: omega, t
    double precision :: pi
    double precision :: atheta, aphi, otheta, ophi

    pi = 3.14159265358979
    omega = 2 * pi / this%T

    if (time .ge. this%start_time) then
        t = time - this%start_time
    else
        t = 0.0d0
    end if
        
    otheta = pi * this%theta_zero / 180.
    ophi = pi * this%phi_zero / 180.
        
    ! Compute spin osition for precession along the z-axis
    sz = cos(otheta)
    sx = cos(ophi + omega*t) * sin(otheta)
    sy = sin(ophi + omega*t) * sin(otheta)

    ! Now rotate along y 
    atheta = pi * this%axis_theta / 180.
    aphi = pi * this%axis_phi / 180.

    sxp = sx*cos(atheta) - sz*sin(atheta)
    syp = sy
    szp = sx*sin(atheta) + sz*cos(atheta)
        
    ! Now rotate along the z
    sx = sxp*cos(aphi) + syp*sin(aphi) 
    sy = -sxp*sin(aphi) + syp*cos(aphi)
    sz = szp
        
    this%s = vector3D(sx, sy, sz)
  end subroutine update

end module precess
