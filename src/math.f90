module math 

  implicit none
  
  double complex, parameter :: IM = (0.0d0, 1.0d0)
  double complex, parameter :: RE = (1.0d0, 0.0d0)
  double complex, parameter :: CZERO = (0.0d0, 0.0d0)

  type vector3D
    double precision :: x, y, z
  end type vector3D

  interface operator (.x.)
    module procedure cross, cross_1D, cross_combine_1D
  end interface

  interface operator (.kron.)
    module procedure kron, kron_real, kron_comb_rc, kron_comb_cr
  end interface

  interface operator(/)
    module procedure vector_divide, vector_divide_1D
  end interface

  interface operator (.dot.)
    module procedure dot, dot_1D, dot_combine, dot_combiner
  end interface 

  interface operator (+)
    module procedure add_vector, add_vector_1D, add_combine, add_combine_r
  end interface

  interface operator (-)
    module procedure subtract_vector, subtract_vector_1D
  end interface

  interface operator (*)
    module procedure multiply_vector_scalar, multiply_vector_scalar_1D, &
                     multipy_combine, multipy_combiner, &
                     combine_scalars_vector, combine_scalars_vectorr
  end interface

  interface normalize
    module procedure normalize, normalize_1D
  end interface normalize

  interface print_vector
    module procedure print_vector, print_vector_1D
  end interface print_vector

contains
  
  type(vector3D) function cross(a, b) result(c) 
    implicit none
    type(vector3D), intent(in) :: a, b

    c%x = a%y * b%z - a%z * b%y
    c%y = a%z * b%x - a%x * b%z
    c%z = a%x * b%y - a%y * b%x

  end function cross


 
  double precision function dot(a, b) result(c)
    implicit none
    type(vector3D), intent(in) :: a, b
    c = a%x * b%x + a%y * b%y + a%z * b%z
  end function dot
    


  subroutine print_vector(c)
    implicit none
    type(vector3D), intent(in) :: c
    integer :: i
    
    write(*, '(3es10.2)', advance='NO') c%x, c%y, c%z 
    write(*, *)
  end subroutine print_vector



  subroutine print_vector_1D(c)
    implicit none 
    type(vector3D), dimension(:), intent(in) :: c
    integer :: i
    do i = 1, size(c)
      call print_vector(c(i))
    end do
  end subroutine print_vector_1D



  subroutine normalize(a)
    implicit none
    type(vector3D), intent(inout) :: a
    double precision :: norm 
    norm = sqrt(a%x**2 + a%y**2 + a%z**2)
    a%x = a%x / norm
    a%y = a%y / norm
    a%z = a%z / norm
  end subroutine normalize



  subroutine normalize_1D(a)
    implicit none
    type(vector3D), dimension(:), intent(inout) :: a
    integer :: n, i
    n = size(a)
    
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, n 
        call normalize(a(i))
    end do
    !$OMP END PARALLEL DO
  end subroutine normalize_1D



  function dot_1D(a, b)
    implicit none
    type(vector3D), dimension(:), intent(in) :: a, b
    double precision, dimension(size(a)) :: dot_1D
    integer :: n, i
    n = size(a)
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, n 
        dot_1D(i) = a(i) .dot. b(i) 
    end do
    !$OMP END PARALLEL DO
  end function dot_1D



  function dot_combine(a, b)
    implicit none
    type(vector3D), dimension(:), intent(in) :: a
    type(vector3D), intent(in) :: b
    double precision, dimension(size(a)) :: dot_combine
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(a) 
        dot_combine(i) = a(i) .dot. b
    end do
    !$OMP END PARALLEL DO
  end function dot_combine
    


  function dot_combiner(b, a)
    implicit none
    type(vector3D), dimension(:), intent(in) :: a
    type(vector3D), intent(in) :: b
    double precision, dimension(size(a)) :: dot_combiner
    dot_combiner = dot_combine(a, b)
  end function dot_combiner
    
    

  function cross_combine_1D(a, b)
    implicit none
    type(vector3D), dimension(:), intent(in) :: a
    type(vector3D), intent(in) :: b
    type(vector3D), dimension(size(a)) :: cross_combine_1D
    integer :: n, i
    n = size(a)
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, n 
        cross_combine_1D(i) = a(i) .x. b 
    end do
    !$OMP END PARALLEL DO
  end function cross_combine_1D



  function cross_1D(a, b)
    implicit none
    type(vector3D), dimension(:), intent(in) :: a, b
    type(vector3D), dimension(size(a)) :: cross_1D
    integer :: n, i
    n = size(a)
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, n 
        cross_1D(i) = a(i) .x. b(i) 
    end do
    !$OMP END PARALLEL DO
  end function cross_1D 



  function pack_vectors(sx, sy, sz)
    implicit none
    double precision, dimension(:), intent(in) :: sx, sy, sz 
    type(vector3D), dimension(size(sx)) :: s, pack_vectors 
    integer :: n, i
    n = size(sx)
    if ((n .ne. size(sy)) .or. (n .ne. size(sz))) & 
      stop "Error! Cant pack vectors of uneven sizes"
    do i = 1, n
       s(i) = vector3D(sx(i), sy(i), sz(i))
    end do
    pack_vectors = s
  end function pack_vectors



  subroutine unpack_vectors(s, sx, sy, sz)
    implicit none
    type(vector3D), dimension(:), intent(in) :: s
    double precision, dimension(size(s)), intent(out) :: sx, sy, sz
    sx(:) = s(:)%x
    sy(:) = s(:)%y
    sz(:) = s(:)%z
  end subroutine unpack_vectors



  function pad_left(a, n)
    type(vector3D), dimension(:), intent(in) :: a
    type(vector3D), dimension(size(a)) :: pad_left
    integer :: n
    
    if ((n < 1) .or. (n > size(a))) &
        stop "Error! Wrong number of padding places in pad_left."
    
    pad_left(1:n) = vector3D(0, 0, 0)
    pad_left(n+1:) = a(1:size(a)-n)
  end function pad_left



  function pad_right(a, n)
    type(vector3D), dimension(:), intent(in) :: a
    type(vector3D), dimension(size(a)) :: pad_right
    integer :: n

    if ((n < 1) .or. (n > size(a))) &
        stop "Error! Wrong number of padding places in pad_right."
    
    pad_right(:)= vector3D(0, 0, 0)
    pad_right(1:size(a)-n) = a(n+1:size(a))
  end function pad_right



  function add_vector(a, b)
    type(vector3D), intent(in) :: a, b
    type(vector3D) :: add_vector 
    add_vector%x = a%x + b%x
    add_vector%y = a%y + b%y
    add_vector%z = a%z + b%z
  end function add_vector



  function add_vector_1D(a, b)
    type(vector3D), dimension(:), intent(in) :: a, b
    type(vector3D), dimension(size(a)) :: add_vector_1D
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(a) 
      add_vector_1D(i) = add_vector(a(i), b(i)) 
    end do
    !$OMP END PARALLEL DO
  end function add_vector_1D



  function subtract_vector(a, b)
    type(vector3D), intent(in) :: a, b
    type(vector3D) :: subtract_vector
    subtract_vector%x = a%x - b%x
    subtract_vector%y = a%y - b%y
    subtract_vector%z = a%z - b%z
  end function subtract_vector



  function subtract_vector_1D(a, b)
    type(vector3D), dimension(:), intent(in) :: a, b
    type(vector3D), dimension(size(a)) :: subtract_vector_1D
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(a) 
      subtract_vector_1D(i) = subtract_vector(a(i), b(i)) 
    end do
    !$OMP END PARALLEL DO
  end function subtract_vector_1D 



  function multiply_vector_scalar(scalar, vector)
    double precision, intent(in) :: scalar 
    type(vector3D), intent(in) :: vector 
    type(vector3D) :: multiply_vector_scalar, prod 
    prod%x = scalar * vector%x
    prod%y = scalar * vector%y
    prod%z = scalar * vector%z
    multiply_vector_scalar = prod
  end function multiply_vector_scalar


  
  function multiply_vector_scalar_1D(scalar, vectors)
    double precision, intent(in) :: scalar 
    type(vector3D), dimension(:), intent(in) :: vectors
    type(vector3D), dimension(size(vectors)) :: mul, multiply_vector_scalar_1D 
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(vectors) 
      mul(i) = multiply_vector_scalar(scalar, vectors(i)) 
    end do
    !$OMP END PARALLEL DO
    multiply_vector_scalar_1D = mul
  end function multiply_vector_scalar_1D



  function multipy_combine(scalars, vectors)
    double precision, dimension(:), intent(in) :: scalars 
    type(vector3D), dimension(:), intent(in) :: vectors
    type(vector3D), dimension(size(vectors)) :: multipy_combine
    integer :: i
    if (size(scalars) .ne. size(vectors)) &
        stop "Error! Can't multiply scalars and vectors. Wrong sizes"
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(vectors) 
      multipy_combine(i) = scalars(i) * vectors(i) 
    end do
    !$OMP END PARALLEL DO
  end function multipy_combine



  function multipy_combiner(vectors, scalars)
    double precision, dimension(:), intent(in) :: scalars 
    type(vector3D), dimension(:), intent(in) :: vectors
    type(vector3D), dimension(size(vectors)) :: multipy_combiner
    integer :: i
    if (size(scalars) .ne. size(vectors)) & 
        stop "Error! Can't multiply scalars and vectors. Wrong sizes"
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(vectors) 
      multipy_combiner(i) = scalars(i) * vectors(i) 
    end do
    !$OMP END PARALLEL DO
  end function multipy_combiner



  function combine_scalars_vector(scalars, vector)
    double precision, dimension(:), intent(in) :: scalars 
    type(vector3D), intent(in) :: vector
    type(vector3D), dimension(size(scalars)) :: combine_scalars_vector
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(scalars) 
      combine_scalars_vector(i) = scalars(i) * vector
    end do
    !$OMP END PARALLEL DO
  end function combine_scalars_vector



  function combine_scalars_vectorr(vector, scalars)
    double precision, dimension(:), intent(in) :: scalars 
    type(vector3D), intent(in) :: vector
    type(vector3D), dimension(size(scalars)) :: combine_scalars_vectorr
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(scalars) 
      combine_scalars_vectorr(i) = scalars(i) * vector
    end do
    !$OMP END PARALLEL DO
  end function combine_scalars_vectorr



  function vector_divide(vector, scalar)
    type(vector3D), intent(in) :: vector
    double precision, intent(in) :: scalar
    type(vector3D) :: vector_divide
    vector_divide%x = vector%x / scalar
    vector_divide%y = vector%y / scalar
    vector_divide%z = vector%z / scalar
  end function vector_divide



  function vector_divide_1D(vectors, scalar) 
    type(vector3D), dimension(:), intent(in) :: vectors
    double precision, intent(in) :: scalar
    type(vector3D), dimension(size(vectors)) :: vector_divide_1D
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(vectors) 
      vector_divide_1D(i) = vector_divide(vectors(i), scalar) 
    end do
    !$OMP END PARALLEL DO
  end function vector_divide_1D



  function add_combine(as, b)
    type(vector3D), dimension(:), intent(in) :: as
    type(vector3D), intent(in) :: b
    type(vector3D), dimension(size(as)) :: add_combine
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(as) 
      add_combine(i) = as(i) + b 
    end do
    !$OMP END PARALLEL DO
  end function add_combine



  function add_combine_r(b, as)
    type(vector3D), dimension(:), intent(in) :: as
    type(vector3D), intent(in) :: b
    type(vector3D), dimension(size(as)) :: add_combine_r
    integer :: i
    !$OMP PARALLEL DO PRIVATE(i)
    do i = 1, size(as) 
      add_combine_r(i) = as(i) + b 
    end do
    !$OMP END PARALLEL DO
  end function add_combine_r



  function kron(a, b)
    double complex, dimension(:, :), intent(in) :: a, b
    double complex, &
      dimension(size(a, 1)*size(b, 1), size(a, 1)*size(b, 1)) :: kron
    integer :: na, nb, i, j, k, m
    na = size(a, 1)
    nb = size(b, 1)
    do i = 1, na
      do j = 1, na
        kron(1+(i-1)*nb:i*nb, 1+(j-1)*nb:j*nb) = a(i, j) * b 
      end do 
    end do
  end function kron



  function kron_comb_rc(a, b) 
    double precision, dimension(:, :), intent(in) :: a
    double complex, dimension(:, :), intent(in) :: b
    double complex, &
      dimension(size(a, 1)*size(b, 1), &
                size(a, 1)*size(b, 1)) :: kron_comb_rc

    kron_comb_rc = ((1.0d0, 0.0d0) * a) .kron. b
  end function kron_comb_rc

  

  function kron_comb_cr(a, b)
    double complex, dimension(:, :), intent(in) :: a
    double precision, dimension(:, :), intent(in) :: b
    double complex, &
      dimension(size(a, 1)*size(b, 1), &
                size(a, 1)*size(b, 1)) :: kron_comb_cr

    kron_comb_cr= a .kron. ((1.0d0, 0.0d0) *b)
  end function kron_comb_cr
    
    

  function kron_real(a, b)
    double precision, dimension(:, :), intent(in) :: a, b
    double precision, &
      dimension(size(a, 1)*size(b, 1), size(a, 1)*size(b, 1)) :: kron_real
    integer :: na, nb, i, j, k, m
    na = size(a, 1)
    nb = size(b, 1)
    do i = 1, na
      do j = 1, na
        kron_real(1+(i-1)*nb:i*nb, 1+(j-1)*nb:j*nb) = a(i, j) * b 
      end do 
    end do
  end function kron_real



  function diag(n, b, k)
    double precision, dimension(:), intent(in) :: b
    double precision, dimension(n, n) :: diag
    integer, intent(in) :: k, n
    integer :: i
    if (abs(k) >= n) &
      stop "Error! Index k out of bounds in function diag"
    if ((abs(k) + size(b)) .ne. n) &
      stop "Error! Wrong length of b in runction diag" 
    diag = 0.0d0
    if (k >= 0) then
      do i = 1, size(b)
        diag(i, i+k) = b(i)
      end do
    else 
      do i = 1, size(b)
        diag(i+abs(k), i) = b(i)
      end do
    end if
  end function diag



  function cdiag(n, b, k)
    double complex, dimension(:), intent(in) :: b
    double complex, dimension(n, n) :: cdiag
    integer, intent(in) :: k, n
    integer :: i
    if (abs(k) >= n) stop "Error! Index k out of bounds in function diag"

    if ((abs(k) + size(b)) .ne. n) & 
      stop "Error! Wrong length of b in function diag" 
    cdiag = (0.0d0, 0.0d0)

    if (k >= 0) then
      do i = 1, size(b)
        cdiag(i, i+k) = b(i)
      end do
    else 
      do i = 1, size(b)
        cdiag(i+abs(k), i) = b(i)
      end do
    end if

  end function cdiag



  function ones(n)
    double precision, dimension(n) :: ones
    integer, intent(in) :: n
    ones = 1.0d0
  end function ones



  function eye(n)
    double precision, dimension(n, n) :: eye
    integer, intent(in) :: n
    eye = diag(n, ones(n), 0)
  end function
    


  function inverse(matrix)
    double complex, dimension(:, :), intent(in) :: matrix
    double complex, dimension(size(matrix, 1), size(matrix, 2)) :: inverse
    double complex, dimension(size(matrix, 1)) :: work
    integer, dimension(size(matrix, 1)) :: ind_pivot
    integer :: n, info
    external ZGETRF
    external ZGETRI

    inverse = matrix
    n = size(matrix, 1)
    
    call ZGETRF(n, n, inverse, n, ind_pivot, info)
    
    if (info .ne. 0) stop "Error! Matrix is numerically singular"
    
    call ZGETRI(n, inverse, n, ind_pivot, work, n, info)
    
    if (info .ne. 0) stop "Error! Matrix inversion failed."
  end function inverse



end module math 
