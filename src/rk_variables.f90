MODULE rksuite_vars

      double precision                       :: TSTART !starting time (psi0)
      double precision                       :: TNOW   !output (time reached by rksuite)
      double precision                       :: TEND   !end time
      integer                    			 :: NEQ    !number of eq (*2 complex->real), defined in rk_init
      integer, parameter                     :: METHOD = 3 !(2 or 3)
      integer                    			 :: LENWRK !in this code set to 16*NEQ
      double precision, parameter            :: TOL = 1.d-6 !(rel error)
      character, parameter                   :: TASK = "U"
      logical, parameter                     :: ERRASS = .false.
      logical                                :: IsRKInit = .false.
      logical                                :: message = .false.
      integer                                :: OUTCH
      double precision                       :: MCHPES
      double precision                       :: DWARF
      double precision, allocatable          :: THRES(:)  !these 3 allocated in rk_init since number of sites parameter (-> NEQ)
      double precision, allocatable          :: WORK(:), YMAX(:)
      integer                                :: IFAIL
      logical                                :: expinit = .false.
END MODULE rksuite_vars 
