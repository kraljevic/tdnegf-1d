module input_output 

  use math, only: vector3D

  interface read_dict
    module procedure read_int, read_bool, read_double, &
                     read_real, read_vector3D, read_string
  end interface

contains
    
  subroutine clear_file(filename)
    character (len=*) :: filename
    open(33, file=filename, status='replace', action='write')
    close(33)
  end subroutine


  subroutine read_int(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=len(name_str)) :: test_str 
    integer :: io
    integer, intent(inout) :: var

    read(unit_num, *, iostat=io) test_str, var 
    if (test_str .ne. name_str .or. io .ne. 0) then 
      print *, "Error! Expected '"//name_str//"', got '"//test_str//"'."
      stop  
    end if
  end subroutine read_int



  subroutine read_bool(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=len(name_str)) :: test_str 
    integer :: io = 0
    logical, intent(inout) :: var

    read(unit_num, *, iostat=io) test_str, var 
    if (test_str .ne. name_str .or. io .ne. 0) then
      print *, "Error! Expected '"//name_str//"', got '"//test_str//"'."
      stop 
    end if
  end subroutine read_bool


  subroutine read_double(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=len(name_str)) :: test_str 
    integer :: io = 0
    double precision, intent(inout) :: var

    read(unit_num, *, iostat=io) test_str, var 
    if (test_str .ne. name_str .or. io .ne. 0) then 
      print *, "Error! Expected '"//name_str//"', got '"//test_str//"'."
      stop 
    end if
  end subroutine read_double


  subroutine read_real(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=len(name_str)) :: test_str 
    integer :: io = 0
    real, intent(inout) :: var

    read(unit_num, *, iostat=io) test_str, var 
    if (test_str .ne. name_str .or. io .ne. 0) then
      print *, "Error! Expected '"//name_str//"', got '"//test_str//"'."
      stop 
    end if
  end subroutine read_real



  subroutine read_vector3D(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=len(name_str)) :: test_str 
    integer :: io = 0
    type(vector3D), intent(inout) :: var

    read(unit_num, *, iostat=io) test_str, var 
    if (test_str .ne. name_str .or. io .ne. 0) then 
      print *, "Error! Expected '"//name_str//"', got '"//test_str//"."
      stop 
    end if 
  end subroutine read_vector3D

  
  subroutine read_string(unit_num, name_str, var)
    integer, intent(in) :: unit_num
    character(len=*), intent(in) :: name_str 
    character(len=400) :: test_str 
    integer :: io = 0
    character(len=400) :: long_var
    character(len=:), allocatable, intent(out) :: var
    integer :: nm

    read(unit_num, *, iostat=io) test_str, long_var 
    if (trim(test_str) .ne. name_str//":" .or. io .ne. 0) then 
      print *, "Error! Expected '"//name_str//"', got '"//trim(test_str)//"."
      stop 
    end if 
    if (len(long_var) .eq. 0) then
      print *, "Error! Empty string passed to variable '"//name_str//"'."
      stop
    end if 
    
    var = trim(adjustl(long_var))
  end subroutine read_string


end module input_output 

