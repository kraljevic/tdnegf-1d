SUBROUTINE create_Gam

        USE global
        implicit none   
        integer:: i,j,k,l
        integer:: io_error
        double complex:: fermi, spectraldensity
      
      
        DO i=1,n_channels
         
          DO k = 1,n_lorentz          
         Gam_greater_L_m(i,k) =  -dcmplx(0.5d0,0.d0) * im * dcmplx(gam_L(k,i),0.d0) * dcmplx(w0_L(k),0.d0) * fermi(-hi_left_m(k) + E_F_left)        
         Gam_greater_L_p(i,k) =  -dcmplx(0.5d0,0.d0) * im * dcmplx(gam_L(k,i),0.d0) * dcmplx(w0_L(k),0.d0) * fermi(-hi_left_p(k) + E_F_left)
         Gam_lesser_L_m(i,k)  =   dcmplx(0.5d0,0.d0) * im * dcmplx(gam_L(k,i),0.d0) * dcmplx(w0_L(k),0.d0) * fermi( hi_left_m(k) - E_F_left)
         Gam_lesser_L_p(i,k)  =   dcmplx(0.5d0,0.d0) * im * dcmplx(gam_L(k,i),0.d0) * dcmplx(w0_L(k),0.d0) * fermi( hi_left_p(k) - E_F_left)  
          END DO
                                
          DO k = n_lorentz+1,k_poles 
              Gam_greater_L_m(i,k) = dcmplx(0.d0,0.d0)
              Gam_greater_L_p(i,k) = dcmplx(0.d0,0.d0)
              Gam_lesser_L_m(i,k)  = dcmplx(0.d0,0.d0)
              Gam_lesser_L_p(i,k)  = dcmplx(0.d0,0.d0)
                  
              DO l = 1,n_lorentz
                 Gam_greater_L_m(i,k) = Gam_greater_L_m(i,k) - dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_left_m(k),dcmplx(eps_L(l),0.d0),dcmplx(w0_L(l),0.d0) ) * dcmplx(gam_L(l,i),0.d0) * dcmplx(R_L(k-n_lorentz),0.d0)   
                     Gam_greater_L_p(i,k) = Gam_greater_L_p(i,k) + dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_left_p(k),dcmplx(eps_L(l),0.d0),dcmplx(w0_L(l),0.d0) ) * dcmplx(gam_L(l,i),0.d0) * dcmplx(R_L(k-n_lorentz),0.d0) 
                     Gam_lesser_L_m(i,k)  = Gam_lesser_L_m(i,k)  - dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_left_m(k),dcmplx(eps_L(l),0.d0),dcmplx(w0_L(l),0.d0) ) * dcmplx(gam_L(l,i),0.d0) * dcmplx(R_L(k-n_lorentz),0.d0) 
                     Gam_lesser_L_p(i,k)  = Gam_lesser_L_p(i,k)  + dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_left_p(k),dcmplx(eps_L(l),0.d0),dcmplx(w0_L(l),0.d0) ) * dcmplx(gam_L(l,i),0.d0) * dcmplx(R_L(k-n_lorentz),0.d0)
                  END DO                        
              END DO              
                       
!*********              

              DO k = 1,n_lorentz                  
                Gam_greater_R_m(i,k) = -dcmplx(0.5d0,0.d0) * im * dcmplx(gam_R(k,i),0.d0) * dcmplx(w0_R(k),0.d0) * fermi(-hi_right_m(k) + E_F_right)
                Gam_greater_R_p(i,k) = -dcmplx(0.5d0,0.d0) * im * dcmplx(gam_R(k,i),0.d0) * dcmplx(w0_R(k),0.d0) * fermi(-hi_right_p(k) + E_F_right)
                Gam_lesser_R_m(i,k)  =  dcmplx(0.5d0,0.d0) * im * dcmplx(gam_R(k,i),0.d0) * dcmplx(w0_R(k),0.d0) * fermi( hi_right_m(k) - E_F_right)  
                Gam_lesser_R_p(i,k)  =  dcmplx(0.5d0,0.d0) * im * dcmplx(gam_R(k,i),0.d0) * dcmplx(w0_R(k),0.d0) * fermi( hi_right_p(k) - E_F_right)  
              END DO  
       
              DO k = n_lorentz+1,k_poles 
              Gam_greater_R_m(i,k) = dcmplx(0.d0,0.d0)
              Gam_greater_R_p(i,k) = dcmplx(0.d0,0.d0)
              Gam_lesser_R_m(i,k)  = dcmplx(0.d0,0.d0)
              Gam_lesser_R_p(i,k)  = dcmplx(0.d0,0.d0)
                  
              DO l = 1,n_lorentz
                 Gam_greater_R_m(i,k) = Gam_greater_R_m(i,k) - dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_right_m(k),dcmplx(eps_R(l),0.d0),dcmplx(w0_R(l),0.d0) ) * dcmplx(gam_R(l,i),0.d0) * dcmplx(R_R(k-n_lorentz),0.d0)   
                     Gam_greater_R_p(i,k) = Gam_greater_R_p(i,k) + dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_right_p(k),dcmplx(eps_R(l),0.d0),dcmplx(w0_R(l),0.d0) ) * dcmplx(gam_R(l,i),0.d0) * dcmplx(R_R(k-n_lorentz),0.d0) 
                     Gam_lesser_R_m(i,k)  = Gam_lesser_R_m(i,k)  - dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_right_m(k),dcmplx(eps_R(l),0.d0),dcmplx(w0_R(l),0.d0) ) * dcmplx(gam_R(l,i),0.d0) * dcmplx(R_R(k-n_lorentz),0.d0) 
                     Gam_lesser_R_p(i,k)  = Gam_lesser_R_p(i,k)  + dcmplx(1.d0,0.d0) / dcmplx(beta,0.d0) * spectraldensity( hi_right_p(k),dcmplx(eps_R(l),0.d0),dcmplx(w0_R(l),0.d0) ) * dcmplx(gam_R(l,i),0.d0) * dcmplx(R_R(k-n_lorentz),0.d0)
                  END DO                        
              END DO  
 
        END DO 
        
!  open(unit=46, file='Gam_output.txt', status='replace', action='write',
!       iostat=io_error)    
!  do i = 1,n_channels
!    do k = 1,k_poles                     
!      write(46,'(15f16.10)') Gam_greater_L_m(i,k)
!    end do
!  end do                  
!  close(unit=46)


END SUBROUTINE create_Gam 
