subroutine save_rho(timestep, filename)
  use global
  double precision, intent(in) :: timestep
  character (len=*), intent(in) :: filename
  character (len=30) :: str_size
  integer :: rho_size
  open(unit=95, file=filename, position='append', action='write')
  rho_size = size(rho)
  write(str_size, *) rho_size
  write(95,'(1f10.5,'//trim(str_size)//'ES21.10E3,'//&
        &trim(str_size)//'ES21.10E3)') timestep, real(rho), aimag(rho)
  close(95)
end subroutine save_rho


subroutine save_rho_and_chargecurr(timestep, filename)
  use global
  implicit none
  double precision, intent(in) :: timestep
  character (len=*), intent(in) :: filename
  double precision :: curr_L, curr_R, curr
  double complex, dimension(2*n,2*n):: prov1, prov2
  integer :: i,j,k,l,m,file, io_error

  double complex :: cdtrace, tempprov1,tempprov2
  character (len=4) :: sqstring

  open(unit=90, file=filename, status='unknown', action='write')
  
  write(sqstring,'(I0)') int(2*n+3) 
    
  call vector_to_matrix_parallelized(rkvec)
                                     

  !OMP PARALLEL
  !OMP WORKSHARE
    prov1(:,:) = dcmplx(0.d0,0.d0)
    prov2(:,:) = dcmplx(0.d0,0.d0)
    Pi_L(:,:) = dcmplx(0.d0,0.d0)   
    Pi_R(:,:) = dcmplx(0.d0,0.d0)
  !OMP END WORKSHARE
  !OMP END PARALLEL                                      
                 
  !$OMP PARALLEL DO PRIVATE(l,m,i,k)  
    DO m=1,2*n   
      DO l=1,2*n    
      tempprov1= dcmplx(0.d0,0.d0)
      tempprov2= dcmplx(0.d0,0.d0)
    
        DO i=1,n_channels   
           DO k=1,k_poles        
           
              prov1(l,m) = (1.d0/hbar) * psi_L(l,i,k) * dconjg(csi_L(m,i,k))  
              prov2(l,m) = (1.d0/hbar) * psi_R(l,i,k) * dconjg(csi_R(m,i,k)) 
           
!               Pi_L(l,m) = Pi_L(l,m) + prov1(l,m) 
!               Pi_R(l,m) = Pi_R(l,m) + prov2(l,m) 
              tempprov1=tempprov1+prov1(l,m) 
              tempprov2=tempprov2+prov2(l,m) 
              
           END DO 
        END DO 
        Pi_L(l,m) = Pi_L(l,m) + tempprov1
        Pi_R(l,m) = Pi_R(l,m) + tempprov2         
      END DO
    END DO                     
  !$OMP END PARALLEL DO                    
         
        
  curr_L=0.d0
  curr_L= 2.d0*real(cdtrace(Pi_L(:,:),2*n))
    
  curr_R=0.d0
  curr_R= 2.d0*real(cdtrace(Pi_R(:,:),2*n))
                
  curr=0.5d0*(curr_L-curr_R)

  curr   = curr   * 2.d0 * pi   
  !2.43413479624d-4 * curr        !Current in Amps i.e. multiply with 
  !(e [C] / hbar [fs])
  curr_L = curr_L * 2.d0 * pi   !2.43413479624d-4 * curr_L
  curr_R = curr_R * 2.d0 * pi   !2.43413479624d-4 * curr_R       


  write(90,'(1f10.2,3ES21.10E3)') timestep, curr, curr_L, curr_R

    
end subroutine save_rho_and_chargecurr


subroutine save_spincurrents(timestep, filename)
  use global
  implicit none
  double precision, intent(in) :: timestep
  character (len=*), intent(in) :: filename
  double precision :: sxcurr_L, sxcurr_R, &
                      sycurr_L, sycurr_R, &
                      szcurr_L, szcurr_R
  double complex, dimension(2*n,2*n):: prov1, prov2
  integer :: i, j, k, l, m, file, io_error, alloc_status, check_alloc
  double complex :: cdtrace
  character (len=4) :: sqstring
  double complex, dimension(:,:), allocatable :: sigma_full_x, &
                                                 sigma_full_y, &
                                                 sigma_full_z

  open(unit=95, file=filename, position='append', action='write')
    
  allocate(sigma_full_x(2*n, 2*n), stat=alloc_status)
  if (alloc_status .ne. 0) check_alloc = check_alloc + 1

  allocate(sigma_full_y(2*n, 2*n), stat=alloc_status)
  if (alloc_status .ne. 0) check_alloc = check_alloc + 1 

  allocate(sigma_full_z(2*n, 2*n), stat=alloc_status)
  if (alloc_status .ne. 0) check_alloc = check_alloc + 1

  if(check_alloc .ne. 0) write(*,*) 'Error allocation spin currents'   
  call vector_to_matrix_parallelized(rkvec)

  !OMP PARALLEL
  !OMP WORKSHARE
    Pi_L(:,:) = dcmplx(0.d0, 0.d0)   
    Pi_R(:,:) = dcmplx(0.d0, 0.d0)
  !OMP END WORKSHARE
  !OMP END PARALLEL                                      

  !$OMP PARALLEL DO PRIVATE(l,m,i,k)  
  do m = 1, 2*n   
    do l = 1, 2*n          
      do i = 1, n_channels   
        do k = 1, k_poles        
          prov1(l,m) = (1.d0/hbar) * psi_L(l,i,k) * dconjg(csi_L(m,i,k))  
          prov2(l,m) = (1.d0/hbar) * psi_R(l,i,k) * dconjg(csi_R(m,i,k)) 
          Pi_L(l,m) = Pi_L(l,m) + prov1(l,m) 
          Pi_R(l,m) = Pi_R(l,m) + prov2(l,m) 
        end do 
      end do      
    end do
  end do                     
  !$OMP END PARALLEL DO                       
    
  call kron(n, 2, 2*n, I_n, sigma_x, sigma_full_x)
  call kron(n, 2, 2*n, I_n, sigma_y, sigma_full_y)
  call kron(n, 2, 2*n, I_n, sigma_z, sigma_full_z)
    
    
  sxcurr_L = 0.d0
  sxcurr_L = 2.d0*real(cdtrace(matmul(sigma_full_x, Pi_L(:,:)), 2*n))
  sxcurr_R = 0.d0
  sxcurr_R = 2.d0*real(cdtrace(matmul(sigma_full_x, Pi_R(:,:)), 2*n))
    
  sycurr_L = 0.d0
  sycurr_L = 2.d0*real(cdtrace(matmul(sigma_full_y, Pi_L(:,:)), 2*n))
  sycurr_R = 0.d0
  sycurr_R = 2.d0*real(cdtrace(matmul(sigma_full_y, Pi_R(:,:)), 2*n))
    
  szcurr_L = 0.d0
  szcurr_L = 2.d0*real(cdtrace(matmul(sigma_full_z, Pi_L(:,:)), 2*n))
  szcurr_R = 0.d0
  szcurr_R = 2.d0*real(cdtrace(matmul(sigma_full_z, Pi_R(:,:)), 2*n))
   
  sxcurr_L = sxcurr_L * 2.d0 * pi  
  sxcurr_R = sxcurr_R * 2.d0 * pi   
    
  sycurr_L = sycurr_L * 2.d0 * pi  
  sycurr_R = sycurr_R * 2.d0 * pi
    
  szcurr_L = szcurr_L * 2.d0 * pi  
  szcurr_R = szcurr_R * 2.d0 * pi  
    
  write(95,'(1f10.2,6ES15.6E3)')  timestep, &
    sxcurr_L, sxcurr_R, sycurr_L, sycurr_R, szcurr_L, szcurr_R
  close(95)

end subroutine save_spincurrents


subroutine save_spindensity(timestep,file)

  use global
  implicit none
  double precision, intent(in) :: timestep
  double complex, dimension(:), allocatable :: spindensity_x, &
                                               spindensity_y, &
                                               spindensity_z
           
  integer :: i, j, k, l, m, file, io_error, alloc_status, check_alloc
  double complex :: cdtrace
  character (len=4) :: sqstring
  write(sqstring,'(I0)') int(3*n) 
    
  allocate(spindensity_x(n), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc=check_alloc+1
  allocate(spindensity_y(n), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc=check_alloc+1 
  allocate(spindensity_z(n), stat= alloc_status)
  if (alloc_status .NE.0) check_alloc=check_alloc+1
  if(check_alloc .NE.0) write(*,*) 'Error allocation spindensity'

  call vector_to_matrix_parallelized(rkvec)
                                     
  spindensity_x(:) = dcmplx(0.d0,0.d0)
  spindensity_y(:) = dcmplx(0.d0,0.d0)
  spindensity_z(:) = dcmplx(0.d0,0.d0)
   
  do i = 1, n 
    spindensity_x(i) = cdtrace(matmul(rho(2*i-1:2*i,2*i-1:2*i),sigma_x), 2)
    spindensity_y(i) = cdtrace(matmul(rho(2*i-1:2*i,2*i-1:2*i),sigma_y), 2)
    spindensity_z(i) = cdtrace(matmul(rho(2*i-1:2*i,2*i-1:2*i),sigma_z), 2)
  end do
!  print*, "SPDENS", spindensity_x(1),spindensity_y(1),spindensity_z(1)
!  print*, "rho",rho(1:2,1:2)
!  write(file,'(19es16.8,t16,19es16.8)') &
!        (real(spindensity_x(i)), i=1,n), &
!        (real(spindensity_y(i)), i=1,n), &
!        (real(spindensity_z(i)), i=1,n)   

!  write(file,'('//trim(sqstring)//'d16.8)') &
!        (real(spindensity_x(i)), i=n_precessing+1,n), &
!        (real(spindensity_y(i)), i=n_precessing+1,n), &
!        (real(spindensity_z(i)), i=n_precessing+1,n)
        
!  write(file,'('//trim(sqstring)//'d16.8)') &
!    ((real(spindensity_x(i))-spindensity_x_stdstat(i)), i=n_precessing+1,n), &
!    ((real(spindensity_y(i))-spindensity_y_stdstat(i)), i=n_precessing+1,n), &
!    ((real(spindensity_z(i))-spindensity_z_stdstat(i)), i=n_precessing+1,n)

  write(file,'('//trim(sqstring)//'d16.8)') &
    (real(spindensity_x(i)), i=1,n), &
    (real(spindensity_y(i)), i=1,n), &
    (real(spindensity_z(i)), i=1,n)
                                                  
end subroutine save_spindensity


subroutine display_parameters(file)
  use global
  implicit none
    
  integer :: i, file
    
  write(file,'(t1,a,t20,i6,t30)') '%#NUMBER OF SITES:',n,'%#without spin'
  write(file,'(t1,a,t20,f6.1)') '%#E_F_left:', E_F_left
  write(file,'(t1,a,t20,f6.1)') '%#E_F_right:', E_F_right
  write(file,'(t1,a,t20,16f6.1)') '%#E(i):', (E(i),i=1,n)     
!  write(file,'(t1,a,t20,10f6.3)') '%#delta(i)', (delta(i),i=1,n-1)   
  write(file,'(t1,a,t20,2f6.2)') '%#Temp', Temp
  write(file,'(t1,a,t20,2i6)')  '%#N_nu_k L/R:', N_poles 
  write(file,'(t1,a,t20,f6.1)') '%#t_0:', t_0
  write(file,'(t1,a,t20,f8.1)') '%#t_end:',  t_end
  write(file,'(t1,a,t20,f6.0)') '%#t_step:', t_step
  write(file,*) '%#gam_L:', gam_L(:,:)
  write(file,*) '%#w0_L:',   w0_L(:) 
  write(file,*) '%#eps_L:',  eps_L(:)
  write(file,*) '%#gam_R:', gam_R(:,:)
  write(file,*) '%#w0_R:',   w0_R(:)
  write(file,*) '%#eps_R:',  eps_R(:)
  write(file,*)

end subroutine display_parameters 


subroutine save_bond_currents(timestep, filename)
  use global
  implicit none
  double precision, intent(in) :: timestep
  character (len=*), intent(in) :: filename
  double complex, dimension(n-1) :: jc, jsx, jsy, jsz
  double complex, dimension(2, 2) :: rho_u, ham_u, rho_d, ham_d, &
                                       cx, cy, cz, cc
  character(5) :: num_string
  integer :: i, j, n_hopp

  call vector_to_matrix_parallelized(rkvec)
  
  jc = 0.d0
  jsx = 0.d0
  jsy = 0.d0 
  jsz = 0.d0
  cc = 0.d0
  cx = 0.d0
  cy = 0.d0
  cz = 0.d0

  do i = 1, n-1
    cc = 0.d0
    cx = 0.d0
    cy = 0.d0
    cz = 0.d0
    rho_u = rho(2*i-1:2*i, 2*i+1:2*i+2) - rho_ozaki(2*i-1:2*i, 2*i+1:2*i+2)
    ham_u = H(2*i+1:2*i+2, 2*i-1:2*i) 

    rho_d = rho(2*i+1:2*i+2, 2*i-1:2*i) - rho_ozaki(2*i+1:2*i+2, 2*i-1:2*i)
    ham_d = H(2*i-1:2*i, 2*i+1:2*i+2) 
    cc = -2d0 * pi * im * (matmul(rho_u, ham_u) - matmul(rho_d, ham_d))
    cx = matmul(sigma_x, cc)
    cy = matmul(sigma_y, cc)
    cz = matmul(sigma_z, cc)

    jc(i) = cc(1, 1) + cc(2, 2)
    jsx(i) = cx(1, 1) + cx(2, 2)
    jsy(i) = cy(1, 1) + cy(2, 2)
    jsz(i) = cz(1, 1) + cz(2, 2)
  end do 
    
  write(num_string, '(I0)') 4*(n-1)

  open(unit=105, file=filename, position='append', action='write')
  write(105, '(f20.10,'//trim(num_string)//'es21.10E3)') timestep, &
        real(jc), real(jsx), real(jsy), real(jsz)
  close(105)

end subroutine save_bond_currents
