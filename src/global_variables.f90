module global   
      
  implicit none 

  integer :: n, n_zero
  integer :: n_channels
  integer::  n_lorentz

  double precision, allocatable, &   ! Onsite energies
      dimension(:) :: E, delta       ! and couplings

  double precision ::  theta_1, phi_1       
  double precision :: alpha
  double precision :: theta_2, phi_2
  double precision :: period
   

  !N to start with rashba
  integer :: N_rash
 ! integer :: n_precessing

            
  ! Lead parameters
  double precision :: E_F_left, E_F_right, E_F_system, &
                      V_bias, delta_tdep_L, delta_tdep_R 
  integer :: N_poles, k_poles

  ! Parameters of Lorentzians
  double precision, allocatable, &
      dimension(:) ::  eps_L, w0_L, eps_R, w0_R 

  ! Parameters of light
  integer ::  light_flag
  
  double precision, allocatable, &
      dimension(:, :) ::  gam_L, gam_R

  double complex, allocatable, &
      dimension(:, :) ::  rho, H, H_spinless, I_n, &
                          rho_ozaki, Pi_L, Pi_R

  double complex, allocatable, dimension(:, :, :, :) :: &
    Omega_LL1, Omega_LL2, Omega_LL3, &
    Omega_LR1, Omega_LR2, Omega_LR3, &           
    Omega_RR1, Omega_RR2, Omega_RR3, &
    Omega_RL1, Omega_RL2, Omega_RL3 
              
  double complex, allocatable, &
      dimension(:, :, :, :) :: csi_loop, psi_loop

  double complex, allocatable, &
      dimension(:, :, :) :: psi_L, psi_R, &
                            summ1_L, summ2_L, summ3_L, &
                            summ1_R, summ2_R, summ3_R, &
                            csi_L, csi_R
                                   
  double complex, allocatable, dimension(:, :) :: drho      

  double complex, allocatable, &
      dimension(:, :, :) :: dpsi_L, dpsi_R

  double complex, allocatable, dimension(:,:,:,:) :: &
    dOmega_LL1, dOmega_LL2, dOmega_LL3, &
    dOmega_LR1, dOmega_LR2, dOmega_LR3, &           
    dOmega_RR1, dOmega_RR2, dOmega_RR3, & 
    dOmega_RL1, dOmega_RL2, dOmega_RL3 

  double complex, allocatable, dimension(:,:) :: &
      Gam_greater_L_m, Gam_greater_L_p, &
      Gam_lesser_L_m, Gam_lesser_L_p, &
      Gam_greater_R_m, Gam_greater_R_p, &
      Gam_lesser_R_m, Gam_lesser_R_p 
                  
  double complex, allocatable, &
      dimension(:) :: hi_left_m, hi_left_p, &
                      hi_right_m, hi_right_p

  double complex, allocatable, &
      dimension(:) :: rkvec, drkvec                  

  ! Matrices utilized to compute the poles of the
  ! Ozaki decomposition of the Fermi function
  double precision, allocatable, &
      dimension(:,:) :: Mat_L, Mat_R              

  double precision, allocatable, &
      dimension(:) :: Eig_val_mat_L, &
                      Eig_val_mat_R, Nu_L, Nu_R, &
                      R_alpha_L, R_alpha_R, R_L, R_R

  double precision, allocatable, &
      dimension(:) :: workspace_for_diag

  integer :: length_workspace, info_success
                
  ! Constants
  double precision, &
      parameter :: pi = 3.141592653589793238d0

  double precision, &
      parameter :: k_Boltzmann = 0.00008617343   !(eV/K) 
                               ! 1.3806504d-23    (J/K)
  double complex, &
      parameter :: im = dcmplx(0.d0, 1.d0)  ! Imaginary unit

  double precision, &
      parameter :: hbar = 0.658211928d0  !(eV*fs)

  double precision :: Temp, beta
  double precision :: t_0, t_end, t_step
  integer :: length_rkvec
        
  ! Pauli matrices and unit matrix
  double complex, dimension(2, 2) :: sigma_x, &
                                     sigma_y, &
                                     sigma_z, &
                                     I_2
  ! Parameters of the spin battery
  double precision :: theta_spin, omega_spin, &
                      J_spin, S_x, S_y, S_z   

  ! Coupling term to classical spins
  double precision :: J_sd              
  double precision, allocatable, &
      dimension(:) :: jsd_llg_to_tdnegf
  
  ! Classical spins 
  double precision, allocatable, &
      dimension(:) :: Sx_classical, &
                      Sy_classical, &
                      Sz_classical 

  integer :: n_precessing 
  double precision :: dw_width  ! DW configuration width
  double precision :: t_long    ! Time before switching LLG
  double precision :: t_bias    ! Time to turn the bias
  integer :: llg_step    ! Run LLG after this num. of steps

  ! Read in steady state. vals of spdens from storage
  double precision, allocatable, &
      dimension(:) :: spindensity_x_stdstat, &
                      spindensity_y_stdstat, &
                      spindensity_z_stdstat 

  type global_options
    logical :: run_llg = .false.
    logical :: replay_llg = .false.
    character(len=:), allocatable :: replay_cspins_file
    logical :: save_cspins = .false.
    logical :: save_spindensity_eq = .false.
    logical :: save_spindensity_neq = .false.
    logical :: save_charge_current = .false.
    logical :: save_spin_current = .false.
    logical :: save_bond_currents = .false.
    logical :: save_rho = .false.
    logical :: preload_rkvec = .false.
    logical :: save_rkvec_end = .false.
    logical :: read_bias = .false.
    character(len=:), allocatable :: eq_solver
    character(len=:), allocatable :: cspin_orientation
    character(len=:), allocatable :: cspins_file
    character(len=:), allocatable :: spindensity_eq_file
    character(len=:), allocatable :: spindensity_neq_file
    character(len=:), allocatable :: charge_current_file
    character(len=:), allocatable :: spin_current_file
    character(len=:), allocatable :: bond_currents_file
    character(len=:), allocatable :: rho_file
    character(len=:), allocatable :: bias_file
    character(len=:), allocatable :: output_path
    character(len=:), allocatable :: load_rkvector_file
    character(len=:), allocatable :: save_rkvec_end_file
    character(len=:), allocatable :: rkvec_file_path 
    character(len=:), allocatable :: selfenergy_file_path 
    integer :: write_freq = 1
  end type global_options

contains
  
  function read_global_options(n) result(gopt)
    use input_output, only: read_dict
    implicit none
    type(global_options) :: gopt 
    integer :: n
    call read_dict(n, 'run_llg', gopt%run_llg)
    call read_dict(n, 'replay_llg', gopt%replay_llg)
    call read_dict(n, 'replay_cspins_file', &
                   gopt%replay_cspins_file)
    call read_dict(n, 'save_cspins', gopt%save_cspins)
    call read_dict(n, 'save_spindensity_eq', &
                   gopt%save_spindensity_eq)
    call read_dict(n, 'save_spindensity_neq', &
                   gopt%save_spindensity_neq)
    call read_dict(n, 'save_charge_current', &
                   gopt%save_charge_current)
    call read_dict(n, 'save_spin_current', &
                   gopt%save_spin_current)
    call read_dict(n, 'save_bond_currents', &
                   gopt%save_bond_currents)
    call read_dict(n, 'save_rho', gopt % save_rho)
    call read_dict(n, 'preload_rkvec', &
                   gopt%preload_rkvec)
    call read_dict(n, 'save_rkvec_end', &
                   gopt%save_rkvec_end)
    call read_dict(n, 'read_bias', gopt % read_bias)
    call read_dict(n, 'eq_solver', gopt%eq_solver)
    call read_dict(n, 'cspin_orientation', &
                   gopt%cspin_orientation)
    call read_dict(n, 'cspins_file', gopt%cspins_file)
    call read_dict(n, 'spindensity_eq_file', &
                   gopt%spindensity_eq_file)
    call read_dict(n, 'spindensity_neq_file', &
                   gopt%spindensity_neq_file)
    call read_dict(n, 'charge_current_file', &
                   gopt%charge_current_file)
    call read_dict(n, 'spin_current_file', &
                   gopt%spin_current_file)
    call read_dict(n, 'bond_currents_file', &
                   gopt%bond_currents_file)
    call read_dict(n, 'rho_file', gopt%rho_file)
    call read_dict(n, 'bias_file', gopt%bias_file)
    call read_dict(n, 'output_path', gopt%output_path)
    call read_dict(n, 'load_rkvector_file', &
                   gopt%load_rkvector_file)
    call read_dict(n, 'save_rkvec_end_file', &
                   gopt%save_rkvec_end_file)
    call read_dict(n, 'rkvec_file_path', &
                   gopt%rkvec_file_path)
    call read_dict(n, 'selfenergy_file_path', &
                   gopt%selfenergy_file_path)
    call read_dict(n, 'write_freq', gopt%write_freq)

  end function read_global_options 
 

end module global 
