SUBROUTINE matrix_to_vec_to_save_end(vector)
 	USE global
	implicit none
	double complex, dimension(length_rkvec) :: vector
	integer:: i,j,k,m,p

	
	
!$OMP PARALLEL DO PRIVATE(k,i,p,j)
    DO k=1,n_lorentz
       DO i=1,n_channels 
         DO p=1,n_lorentz
              DO j=1,n_channels
               m = j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
               vector(m) = Omega_LL1(j,p,i,k)
           END DO
         END DO
       END DO 
    END DO 
!$OMP END PARALLEL DO    


!$OMP PARALLEL DO PRIVATE(k,i,p,j)       
    DO k=1,n_lorentz
        DO i=1,n_channels
           DO p=1,k_poles-n_lorentz
             DO j=1,n_channels
                m = n_channels**2 * n_lorentz**2 + j + n_channels * ( (p-1) + (k_poles-n_lorentz) * ( (i-1) + n_channels*(k-1) ))
                vector(m) = Omega_LL2(j,p,i,k)
             END DO
          END DO
       END DO 
    END DO 
!$OMP END PARALLEL DO        
       
!$OMP PARALLEL DO PRIVATE(k,i,p,j)
    DO k=1,k_poles-n_lorentz
      DO i=1,n_channels
        DO p=1,n_lorentz
            DO j=1,n_channels
               m = n_channels**2 * (n_lorentz**2 + n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
               vector(m) = Omega_LL3(j,p,i,k)
            END DO
         END DO
       END DO 
     END DO
!$OMP END PARALLEL DO        
              
!$OMP PARALLEL DO PRIVATE(k,i,p,j)    
     DO k=1,n_lorentz
        DO i=1,n_channels
             DO p=1,n_lorentz
                 DO j=1,n_channels	
                  m = n_channels**2 * (n_lorentz**2 + 2*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) )) 
                  vector(m) = Omega_LR1(j,p,i,k)
               END DO
           END DO
        END DO 
     END DO 
!$OMP END PARALLEL DO        
        
!$OMP PARALLEL DO PRIVATE(k,i,p,j)
     DO k=1,n_lorentz   
         DO i=1,n_channels
            DO p=1,k_poles-n_lorentz
               DO j=1,n_channels	 
                   m = n_channels**2 * (2*n_lorentz**2 + 2*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + (k_poles-n_lorentz) * ( (i-1) + n_channels*(k-1) ))
                   vector(m) = Omega_LR2(j,p,i,k)
                   END DO
               END DO
           END DO 
        END DO 
!$OMP END PARALLEL DO

!$OMP PARALLEL DO PRIVATE(k,i,p,j)
      DO k=1,k_poles-n_lorentz    
          DO i=1,n_channels
                 DO p=1,n_lorentz
                     DO j=1,n_channels	
                        m = n_channels**2 * (2*n_lorentz**2 + 3*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
                        vector(m) = Omega_LR3(j,p,i,k)
                     END DO
               END DO
           END DO 
        END DO        
!$OMP END PARALLEL DO       
               
!$OMP PARALLEL DO PRIVATE(k,i,p,j)               
        DO k=1,n_lorentz
            DO i=1,n_channels 
               DO p=1,n_lorentz
                     DO j=1,n_channels	
                        m = n_channels**2 * (2*n_lorentz**2 + 4*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
                        vector(m) = Omega_RR1(j,p,i,k)
                     END DO
                END DO
           END DO 
        END DO         
!$OMP END PARALLEL DO       
           
!$OMP PARALLEL DO PRIVATE(k,i,p,j)                          
        DO k=1,n_lorentz   
           DO i=1,n_channels
                DO p=1,k_poles-n_lorentz
                   DO j=1,n_channels	
                     m = n_channels**2 * (3*n_lorentz**2 + 4*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + (k_poles-n_lorentz) * ( (i-1) + n_channels*(k-1) ))
                     vector(m) = Omega_RR2(j,p,i,k)
                     END DO
               END DO
           END DO 
        END DO   
!$OMP END PARALLEL DO       

!$OMP PARALLEL DO PRIVATE(k,i,p,j)                          
        DO k=1,k_poles-n_lorentz 
          DO i=1,n_channels
              DO p=1,n_lorentz
                  DO j=1,n_channels	
                        m = n_channels**2 * (3*n_lorentz**2 + 5*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
                        vector(m) = Omega_RR3(j,p,i,k)
                     END DO
               END DO
           END DO 
        END DO        
!$OMP END PARALLEL DO

!$OMP PARALLEL DO PRIVATE(k,i,p,j)                         
        DO k=1,n_lorentz
           DO i=1,n_channels
              DO p=1,n_lorentz
                    DO j=1,n_channels	
                      m = n_channels**2 * (3*n_lorentz**2 + 6*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
                      vector(m) = Omega_RL1(j,p,i,k)
                     END DO
                END DO
           END DO 
        END DO 
!$OMP END PARALLEL DO

!$OMP PARALLEL DO PRIVATE(k,i,p,j)                         
        DO k=1,n_lorentz
           DO i=1,n_channels
            DO p=1,k_poles-n_lorentz
                       DO j=1,n_channels	  
                        m = n_channels**2 * (4*n_lorentz**2 + 6*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + (k_poles-n_lorentz) * ( (i-1) + n_channels*(k-1) ))
                        vector(m) = Omega_RL2(j,p,i,k)
                     END DO
               END DO
           END DO 
        END DO 
!$OMP END PARALLEL DO

!$OMP PARALLEL DO PRIVATE(k,i,p,j)
        DO k=1,k_poles-n_lorentz 
               DO i=1,n_channels 
                     DO p=1,n_lorentz
                       DO j=1,n_channels	
                        m = n_channels**2 * (4*n_lorentz**2 + 7*n_lorentz*(k_poles-n_lorentz)) + j + n_channels * ( (p-1) + n_lorentz * ( (i-1) + n_channels*(k-1) ))
                        vector(m) = Omega_RL3(j,p,i,k)
                     END DO
               END DO
           END DO 
        END DO
!$OMP END PARALLEL DO       

!$OMP PARALLEL DO PRIVATE(k,j,i)  
     DO k=1,k_poles	
          DO j=1,n_channels	
	        DO i=1,2*n
	        m = n_channels**2 * (4*n_lorentz**2 + 8*n_lorentz*(k_poles-n_lorentz)) + i + 2*n * ( (j-1) + n_channels * (k-1) )
	        vector(m) = psi_L(i,j,k)
		  END DO
       END DO
    END DO			   
!$OMP END PARALLEL DO                

!$OMP PARALLEL DO PRIVATE(k,j,i)        
    DO k=1,k_poles	
       DO j=1,n_channels	
		  DO i=1,2*n
		    m = n_channels**2 * (4*n_lorentz**2 + 8*n_lorentz*(k_poles-n_lorentz)) + 2*n*n_channels*k_poles + i + 2*n * ( (j-1) + n_channels * (k-1) )
		    vector(m) = psi_R(i,j,k)
		  END DO
       END DO
    END DO	
!$OMP END PARALLEL DO        
    
!$OMP PARALLEL DO PRIVATE(i,j)      
    DO j=1,2*n
        DO i=1,2*n
            m = n_channels**2 * (4*n_lorentz**2 + 8*n_lorentz*(k_poles-n_lorentz)) + 2*2*n*n_channels*k_poles + i + 2*n*(j-1)
            vector(m) = rho(i,j)
        END DO
    END DO
!$OMP END PARALLEL DO   

return
END SUBROUTINE matrix_to_vec_to_save_end
