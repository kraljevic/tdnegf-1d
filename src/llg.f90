module llg

 use math
 use constants, only: MBOHR, GAMMA_R
 use input_output, only: read_dict


  type llg_parameters

    integer :: n_sites = -1
    integer :: nt = 1
    double precision :: dt = 0.1
    type(vector3D) :: h0 = vector3D(0.0, 0.0, 0.0)
    double precision :: j_exc = 0.0
    double precision :: g_lambda = 0.0
    double precision :: j_sd = 0.0
    double precision :: j_dmi = 0.0
    double precision :: j_ani = 0.0
    double precision :: j_dem = 0.0
    double precision :: js_pol = 0.0
    double precision :: js_ana = 0.0
    double precision :: thop = 0.0
    type(vector3D) :: e = vector3D(0.0, 0.0, 0.0)
    type(vector3D) :: e_demag = vector3D(0.0, 0.0, 0.0)
    double precision, allocatable, dimension(:) :: js_exc
    double precision, allocatable, dimension(:) :: js_sd
    double precision, allocatable, dimension(:) :: js_ani
    double precision, allocatable, dimension(:) :: js_dem
    double precision :: p_theta = 0.0
    double precision :: p_phi = 0.0
  end type llg_parameters


contains

  subroutine read_spins(time_to_read, spins, filename)
    double precision, intent(in) :: time_to_read
    double precision :: time
    type(vector3D), dimension(:), intent(inout) :: spins
    type(vector3D), dimension(size(spins)) :: spins_from_file
    character (len=*), intent(in) :: filename
    character (len=20) :: spinformat
    integer :: i, io_error

    open(33, file=filename, action='read')
    write(spinformat, '(A, I3, A)') '(', 3*size(spins) + 1, &
          'ES21.10E3)'
    do
      read(33, spinformat, iostat=io_error) time, spins_from_file
      if (io_error > 0) then
        stop "Error! Problem reading cspins replay file."
      else if (io_error .eq. 0) then
        if (abs(time - time_to_read) .lt. 1.e-8) then
          spins = spins_from_file
          exit
        endif
      else
        stop "Error reading spins! Couldn't find the given time moment."
      end if
    end do

    close(33)
  end subroutine read_spins


  subroutine save_spins(time, spins, filename)
    double precision, intent(in) :: time
    type(vector3D), dimension(:), intent(in) :: spins
    character (len=*), intent(in) :: filename
    character (len=20) :: spinformat
    integer :: i

    open(33, file=filename, position='append', action='write')
    write(spinformat, '(A, I3, A)') '(', 3*size(spins) + 1, &
          'ES21.10E3)'
    write(33, spinformat, advance='NO') time, spins
    close(33)
  end subroutine save_spins


  function read_llg_params(n) result(p)
    integer, intent(in) :: n
    type(llg_parameters) :: p

    call read_dict(n, 'n_sites', p % n_sites)
    call read_dict(n, 'nt', p % nt)
    call read_dict(n, 'dt', p % dt)
    call read_dict(n, 'h0', p % h0)
    call read_dict(n, 'j_exc', p % j_exc)
    call read_dict(n, 'g_lambda', p % g_lambda)
    call read_dict(n, 'j_sd', p % j_sd)
    call read_dict(n, 'j_dmi', p % j_dmi)
    call read_dict(n, 'j_ani', p % j_ani)
    call read_dict(n, 'j_dem', p % j_dem)
    call read_dict(n, 'e', p % e)
    call read_dict(n, 'e_demag', p % e_demag)
    call read_dict(n, 'js_pol', p%js_pol)
    call read_dict(n, 'js_ana', p%js_ana)
    call read_dict(n, 'thop', p%thop)
    call read_dict(n, 'p_theta', p%p_theta)
    call read_dict(n, 'p_phi', p%p_phi)

  end function read_llg_params



  subroutine read_llg_file(params, filename)
    type(llg_parameters), intent(inout) :: params
    character (len=*) :: filename
    open(unit=33, file=filename, action='read', status='unknown')
    params = read_llg_params(33)
    close(33)
  end subroutine read_llg_file



  subroutine propagate_llg(spins, spin_density, params)
    implicit none
    type(vector3D), dimension(:), intent(inout) :: spins
    type(vector3D), dimension(:), intent(in) :: spin_density
    type(vector3D), dimension(size(spins)) :: ds_pred, spins_pred
    type(vector3D), dimension(size(spins)) :: ds_corr, spins_corr
    type(llg_parameters), intent(in) :: params
    integer :: i

    call normalize(spins)

    do i = 1, params%nt
! write(*, *) "Running LLG for time", i
      call heun(spins, spin_density, params, ds_pred, spins_pred)
      call heun(spins_pred, spin_density, params, ds_corr, spins_corr)

      spins = spins + (0.5 * params%dt * (ds_corr + ds_pred))

      call normalize(spins)
    end do

  end subroutine propagate_llg


  subroutine heun(spins, el_spins, params, del_spins, spins_new)
    implicit none
    type(vector3D), dimension(:), intent(in) :: spins, el_spins
    type(llg_parameters), intent(in) :: params
    type(vector3D), dimension(:), intent(out) :: del_spins, spins_new
    type(vector3D), dimension(size(el_spins)) :: hef, sh, shh
    integer :: n

    n = size(spins)
    hef = heff(spins, el_spins, params)
    sh = spins .x. hef
    shh = spins .x. sh

    del_spins = (-GAMMA_R / (1.0 + params%g_lambda**2)) * &
                 (sh + params%g_lambda * shh)

    spins_new = spins + params%dt * del_spins
    call normalize(spins_new)
  end subroutine heun


  function heff(spins, spin_density, p)
    implicit none
    type(vector3D), dimension(:), intent(in) :: spins, spin_density
    type(vector3D), dimension(size(spins)) :: spins_p1, spins_m1
    type(vector3D), dimension(size(spins)) :: heff
    type(llg_parameters), intent(in) :: p
    double precision, dimension(size(spins)) :: jexc_m1, jexc_p1

    ! A note about exchange couplings. We add two sites 
    ! one to the left, and one to the right, so for a 4-site 
    ! system, the hopping between the neighbors is implemented
    ! as following

    ! spins_m1............0 1 2 3..................o o o
    ! jexc_m1.............0 1 2 3..................| | |
    ! spins...............1 2 3 4......OR........o o o o
    ! jexc_p1.............1 2 3 0................| | | |
    ! spins_p1............2 3 4 0................o o o

    spins_p1 = pad_right(spins, 1)
    spins_m1 = pad_left(spins, 1)

    jexc_m1 = 0.0d0
    jexc_m1(2:size(spins)) = p%js_exc

    jexc_p1 = 0.0d0
    jexc_p1(1:size(spins)-1) = p%js_exc

    heff = vector3D(0, 0, 0)
    heff = heff + (jexc_m1 * spins_m1)
    heff = heff + (jexc_p1 * spins_p1)

    heff = heff + (p%js_sd * spin_density)

    heff = heff + (p%j_dmi * &
             (spins_p1-spins_m1) .x. vector3D(0.0, 1.0, 0.0))
    heff = heff + (p%js_ani * ((spins .dot. p%e) * p%e))
    heff = heff - (p%js_dem * ((spins .dot. p%e_demag) * p%e_demag))
    heff = heff / MBOHR
    heff = heff + p%h0
  end function heff


end module llg

