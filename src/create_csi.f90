SUBROUTINE create_csi

  USE global
  integer:: i, j, k 
 
  
     DO i=1,2*n

        DO k=1,k_poles   
        
          if  (i .EQ. 1)  then 
             csi_L(i,1,k) = dcmplx(1.d0,0.d0)
          else 
                csi_L(i,1,k) = dcmplx(0.d0,0.d0)
          end if 
          
          if ( i .EQ. 2) then
             csi_L(i,2,k) = dcmplx(1.d0,0.d0)
          else 
                csi_L(i,2,k) = dcmplx(0.d0,0.d0)
          end if    
          
!           if ( (i .EQ. 2*n) .AND. (n .NE. 1) ) then 
!              csi_L(i,1,1,k)   = dcmplx(0.d0,0.d0)
!              csi_L(i-1,1,1,k) = dcmplx(0.d0,0.d0)
!              csi_R(i,1,1,k)   = dcmplx(1.d0,0.d0)   !dcmplx( (1.d0/sqrt(dble(n))) , 0.d0 ) * dcmplx(1.d0,0.d0)
!              csi_R(i-1,1,1,k) = dcmplx(1.d0,0.d0)
!           end if 
!     
            
        END DO
        
        
!******

        DO k=1,k_poles   

          if ( (i .EQ. 2*n-1) ) then 
             csi_R(i,1,k) = dcmplx(1.d0,0.d0)  
          else    
                  csi_R(i,1,k) = dcmplx(0.d0,0.d0)       
          end if 
          
          if ( (i .EQ. 2*n) ) then 
             csi_R(i,2,k) = dcmplx(1.d0,0.d0)  
          else 
                csi_R(i,2,k) = dcmplx(0.d0,0.d0)  
          end if 
          
        
!              
!              else 
!                csi_R(1,1,2,k) = dcmplx(1.d0,0.d0) !dcmplx( (1.d0/sqrt(dble(n))) , 0.d0 ) * dcmplx(1.d0,0.d0)
!                csi_R(2,1,2,k) = dcmplx(1.d0,0.d0) !dcmplx( (1.d0/sqrt(dble(n))) , 0.d0 ) * dcmplx(1.d0,0.d0)
!              end if        
!           end if 
!           
!           if ( (i .EQ. 2*n) .AND. (n .NE. 1) ) then 
!              csi_L(i,1,2,k)   = dcmplx(0.d0,0.d0)
!              csi_L(i-1,1,2,k) = dcmplx(0.d0,0.d0)
!              csi_R(i,1,2,k)   = dcmplx(1.d0,0.d0)   !dcmplx( (1.d0/sqrt(dble(n))) , 0.d0 ) * dcmplx(1.d0,0.d0)
!              csi_R(i-1,1,2,k) = dcmplx(1.d0,0.d0)
!           end if 
!        
!           if ( (i .NE. 1) .AND. (i .NE. 2) .AND. (i .NE. (2*n-1)) .AND. (i .NE. 2*n) ) then
!              csi_L(i,1,2,k) = dcmplx(0.d0,0.d0)
!              csi_R(i,1,2,k) = dcmplx(0.d0,0.d0)
!           end if     
            
        END DO
  
     END DO
     
!   write(*,*)  'spin up channel'     
!   write(*,*)  'csi_L_up'
!   write(*,*)  csi_L(:,1,1)
!   write(*,*)  'csi_L_down'
!   write(*,*)  csi_L(:,2,1)   
! 
!   write(*,*)  'spin down channel'
!   write(*,*)  'csi_R_up'
!   write(*,*)  csi_R(:,1,1)
!   write(*,*)  'csi_R_down'
!   write(*,*)  csi_R(:,2,1)   

 
  
END SUBROUTINE create_csi 
