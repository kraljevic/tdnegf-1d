SUBROUTINE create_hi

    USE global
    implicit none	
    integer:: i,j,k

        DO i=1,n_lorentz        
          hi_left_m(i) = eps_L(i) - im*w0_L(i)   
          hi_left_p(i) = eps_L(i) + im*w0_L(i)   

          hi_right_m(i) = eps_R(i) - im*w0_R(i)  
          hi_right_p(i) = eps_R(i) + im*w0_R(i)  
        END DO 

        DO i=n_lorentz+1,k_poles              
             hi_left_m(i) = E_F_left - im / (Nu_L(i-n_lorentz)*beta)  
             hi_left_p(i) = E_F_left + im / (Nu_L(i-n_lorentz)*beta)              

             hi_right_m(i) = E_F_right - im / (Nu_R(i-n_lorentz)*beta)  
             hi_right_p(i) = E_F_right + im / (Nu_R(i-n_lorentz)*beta)  
        END DO

END SUBROUTINE create_hi 
