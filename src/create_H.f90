subroutine create_H(timep, Ax)
  use global
  implicit none   
  double precision :: factor, timep, Ax
  double complex :: l_f
  integer :: i, j, k, l, m, tdep
  integer :: io_error
  character (len=4) :: sqstring
  DO i=1,n
    DO j=1,n
      H_spinless(i,j) = dcmplx(0.d0,0.d0)
    END DO 
  END DO 
    
  DO i=1,2*n
    DO j=1,2*n
      H(i,j) = dcmplx(0.d0,0.d0)
    END DO 
  END DO

  l_f = exp( (im/hbar) * Ax )
    
    
  DO l=1,n
    DO m=1,n

      IF (l==m) THEN   
        H_spinless(l,m) = dcmplx(E(l),0.d0)
      ELSE IF ( ((l+1)==m).AND.(l .NE. n) ) THEN
        H_spinless(l,m) = -delta(l)*l_f
      ELSE IF ( (l==(m+1)).AND.(m .NE. n) ) THEN
        H_spinless(l,m) = -delta(m)*l_f
      ELSE                                 
        H_spinless(l,m) = dcmplx(0.d0,0.d0)
      END IF 


    END DO
  END DO           
    
  call kron(n,2,n*2,H_spinless,I_2,H)

  ! add the coupling term to LLG                                    
  DO l=n_precessing+1,n
    H(2*l-1:2*l,2*l-1:2*l) = H(2*l-1:2*l,2*l-1:2*l) &
        - sigma_x * dcmplx(jsd_llg_to_tdnegf(l) * &
                           Sx_classical(l-n_precessing), 0.d0) &
        - sigma_y * dcmplx(jsd_llg_to_tdnegf(l) * &
                           Sy_classical(l-n_precessing), 0.d0) &
        - sigma_z * dcmplx(jsd_llg_to_tdnegf(l) * &
                           Sz_classical(l-n_precessing), 0.d0)  
  END DO 
    
  write (sqstring,'(I0)') (n)
        
END SUBROUTINE create_H 
