SUBROUTINE set_initial_values

        USE global 
        implicit none
        integer:: i, j, k, p

!       delta_tdep_L = 0.d0
!       delta_tdep_R = 0.d0
        
        DO i=1,2*n 
          DO j=1,2*n

              IF((i==1) .AND. (j==1)) THEN            
                 rho(i,j)=dcmplx(0.d0,0.d0)           !one electron
              !IF (i==j) THEN  
              !  rho(i,j)=dcmplx(0.5d0,0.d0)          !0.5 occupation probability for each site        
              ELSE
                rho(i,j)=dcmplx(0.d0,0.d0)
              END IF             
    
              Pi_L(i,j)=dcmplx(0.d0,0.d0)
              Pi_R(i,j)=dcmplx(0.d0,0.d0)
              
           END DO 
        END DO       
        
        drho=rho  
                
!>>>>>>>>>>>>>>>>>>>>

        DO i=1,n_channels     
          DO j=1,n_channels
          

              DO k=1,n_lorentz
                 DO p=1,n_lorentz
                     Omega_LL1(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_LR1(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LL1(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LR1(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 

              DO k=1,n_lorentz
                 DO p=1,k_poles-n_lorentz
                     Omega_LL2(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_LR2(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LL2(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LR2(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 
              
              DO k=1,k_poles-n_lorentz
                 DO p=1,n_lorentz
                     Omega_LL3(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_LR3(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LL3(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_LR3(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 
              
              
              DO k=1,n_lorentz
                 DO p=1,n_lorentz
                     Omega_RR1(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_RL1(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RR1(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RL1(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 

              DO k=1,n_lorentz
                 DO p=1,k_poles-n_lorentz
                     Omega_RR2(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_RL2(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RR2(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RL2(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 
              
              DO k=1,k_poles-n_lorentz
                 DO p=1,n_lorentz
                     Omega_RR3(i,j,k,p) = dcmplx(0.d0,0.d0)                
                     Omega_RL3(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RR3(i,j,k,p) = dcmplx(0.d0,0.d0)
                    dOmega_RL3(i,j,k,p) = dcmplx(0.d0,0.d0)
                 END DO
              END DO 
              
              
          END DO 
        END DO 
        
        
        DO i=1,2*n
          DO j=1,n_channels
          
            DO k=1,k_poles
                 psi_L(i,j,k) = dcmplx(0.d0,0.d0) 
                 psi_R(i,j,k) = dcmplx(0.d0,0.d0)
                dpsi_L(i,j,k) = dcmplx(0.d0,0.d0) 
                dpsi_R(i,j,k) = dcmplx(0.d0,0.d0)
                
            END DO
            
            DO k=1,n_lorentz
                summ1_L(i,j,k) = dcmplx(0.d0,0.d0) 
                summ2_L(i,j,k) = dcmplx(0.d0,0.d0)
                summ1_R(i,j,k) = dcmplx(0.d0,0.d0) 
                summ2_R(i,j,k) = dcmplx(0.d0,0.d0) 
            END DO
            
            DO k=1,k_poles-n_lorentz
                summ3_L(i,j,k) = dcmplx(0.d0,0.d0) 
            END DO 
            
            DO k=1,k_poles-n_lorentz
                summ3_R(i,j,k) = dcmplx(0.d0,0.d0) 
            END DO 
                       
          END DO 
        END DO

        call matrix_to_vector_parallelized(rkvec)
  
          
END SUBROUTINE set_initial_values 
