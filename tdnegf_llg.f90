! TD-NEGF method from Phys. Rev. B 80, 245311 (2009) modified
! to its formulation in terms of wave-vectors as in
! New J. Phys. 18, 093044 (2016).
!*************************************************************
! 1D system, spinfull case.
! Lorentz level-width functions.
! Ozaki spectral decomposition of the Fermi function.
! Parallelized with OpenMP.


program tdnegf_1D_spinfull

    use global
    use ozaki_integrator, only: init_ozaki, init_denis, &
                              spindensity_eq, &
                              spindensity
    use green_1d, only: green
    use config, only: configure
    use precess, only: prec_spin
    use llg, only: llg_parameters, propagate_llg, &
                 vector3D, pack_vectors, &
                 unpack_vectors, save_spins, read_spins
    use input_output, only: clear_file
    use math, only: operator (-)

    implicit none
    include 'git_version.inc' ! File in which to store the hash
    integer :: i, j, io_error, k, l, kk, jj, pind
    integer :: time_1, time_2, cot_rate, cot_max
    double precision :: backuptime
    double precision :: real_part, ima_part
    double precision :: energ, delta_en
    double complex :: sum_sp_dens, spectraldensity
    double precision, allocatable, dimension(:) :: d_left, d_right
    double precision, allocatable, dimension(:) :: Ax_t
    character (len=4) :: indic_folder
    character (len=:), allocatable :: parameters_file
    type(vector3D), allocatable, dimension(:) :: cspins
    type(vector3D), allocatable, dimension(:) :: espins
    type(vector3D), allocatable :: diff(:), s_neq(:)
    real :: energy_llg
    type(global_options) :: gopt
    type(llg_parameters) :: llg_param
    integer :: count_llg
    type(prec_spin), allocatable, dimension(:) :: pr_spins
    character(len=*), parameter :: version = '0.17.3'

    ! call CPU_time(cputime1)
    call system_clock(time_1, cot_rate, cot_max)

    parameters_file = cmd_arg(1)

    if (parameters_file == '-v' .or. &
      parameters_file == '--version') then
    print *, version
    stop
    end if

    if (parameters_file == '-c' .or. &
      parameters_file == '--commit') then
    print *, git_hash
    stop
    end if

    if (parameters_file == '-h' .or. &
      parameters_file == '--help') then
    print *, "Time-dependent NEGF code"
    print *, "-----------------------"
    print *, "Run with ./tdnegf_llg parameters_file"
    print *, "Instead of parameters file the following options"
    print *, "are available:"
    print *, " -v Show version"
    print *, " -c Show git hash"
    print *, " -h Show this help message"
    print *, ""
    print *, "Developed by Marko Petrovic, and Bogdan S. Popescu"
    print *, "mpetrovic.ks@gmail.com"
    stop
    end if

    ! Beside LLG parameters and other options, this function
    ! reads global variables (which is not so obvious)
    call get_parameters(parameters_file, llg_param, gopt)
    E_F_left = E_F_system + 0.5*V_bias
    E_F_right = E_F_system - 0.5*V_bias
    
    print *, "Git hash: "
    print *, git_hash
    print *, "Version: "
    print *, version
    allocate(cspins(n))
    allocate(espins(n))
    allocate(d_left(int(t_end/t_step)))
    allocate(d_right(int(t_end/t_step)))
    allocate(Ax_t(int(t_end/t_step)))
    allocate(diff(n))
    allocate(s_neq(n))
    allocate(llg_param%js_exc(n-1))   ! Position dependent Jex
    allocate(llg_param%js_sd(n))      ! Position dependent Jsd
    allocate(llg_param%js_dem(n))     ! Position dependent demag.
    allocate(llg_param%js_ani(n))     ! Position dependent anis.
    allocate(jsd_llg_to_tdnegf(n))    ! Position dependent Jsd

    llg_param%js_exc = llg_param%j_exc
    llg_param%js_ani = llg_param%j_ani
    llg_param%js_dem = llg_param%j_dem
    llg_param%js_sd = llg_param%j_sd  ! From TDNEGF to LLG
    jsd_llg_to_tdnegf = J_sd          ! From LLG to TDNEGF

    s_neq = vector3D(0, 0, 0)
    diff = vector3D(0, 0, 0)
    espins = vector3D(0, 0, 0)
    Ax_t = 0.0

    write(indic_folder,'(I0)') int(n)

    call const_mat()
    call dsyev('V','U', 2*N_poles, Mat_L, 2*N_poles, &
             Eig_val_mat_L, workspace_for_diag, &
             length_workspace, info_success)

    if (info_success .ne. 0) &
    write(*,*) 'Error in diagonalization'

    call dsyev('V','U', 2*N_poles, Mat_R, 2*N_poles, &
             Eig_val_mat_R, workspace_for_diag, &
             length_workspace, info_success)

    if (info_success .ne. 0) &
    write(*,*) 'Error in diagonalization'

    call calc_R()
    call construct_nu()

    call configure(gopt%cspin_orientation, llg_param, pr_spins)

    spindensity_x_stdstat = 0.0
    spindensity_y_stdstat = 0.0
    spindensity_z_stdstat = 0.0

    call create_H(0.0, 0,0)
    call create_hi
    call create_Gam
    call create_csi

    if (gopt%preload_rkvec) then
    print *, "Loading " // gopt%rkvec_file_path // &
             gopt%load_rkvector_file

    open(unit=33, file=gopt%rkvec_file_path// &
         gopt%load_rkvector_file, &
         status='old', action='read', iostat=io_error)

    if (io_error .ne. 0) &
        stop 'Error! Can not read starting rkvector'

    do i = 1, length_rkvec
      read(33,'(2es16.8)') real_part, ima_part
      rkvec(i) = dcmplx(real_part,ima_part)
    end do

    close(unit=33)
    end if

    call vector_to_matrix_parallelized(rkvec)
    call rk_init

    if (gopt%eq_solver .eq. 'ozaki') then
      print *, "Initializing ozaki EQ Solver"
      call init_ozaki(200)
    else if (gopt%eq_solver .eq. 'denis') then
      print *, "Initializing denis EQ Solver"
      call init_denis(E_F_system, Temp, -3.0d0, 21)
    else
      stop 'Error! Unknown EQ solver.'
    end if

    if (t_long > t_end) &
    stop "Error! Termalization time exeeds total time."

    count_llg = 1

    call clear_file(gopt%output_path // gopt%cspins_file)
    call clear_file(gopt%output_path // &
                  gopt%spindensity_eq_file)
    call clear_file(gopt%output_path // &
                  gopt%spindensity_neq_file)
    call clear_file(gopt%output_path // gopt%rho_file)
    call clear_file(gopt%output_path // &
                  gopt%charge_current_file)
    call clear_file(gopt%output_path // &
                  gopt%spin_current_file)
    call clear_file(gopt%output_path // &
                  gopt%bond_currents_file)

    diff = vector3D(0, 0, 0)

    energy_llg = 0.5*(E_F_left + E_F_right)

    if (gopt % read_bias) then
        call read_bias_file(gopt%bias_file, d_left, d_right)
    end if
    
    if (light_flag==1) then
        call read_bias_file('light_Ax.txt', Ax_t, Ax_t)
    end if

    ! --------------------------------------
    ! Start the time loop
    ! --------------------------------------

    do i = 1, int(t_end/t_step)

        if (gopt % read_bias) then
          delta_tdep_L = d_left(i)
          delta_tdep_R = d_right(i)
        else
          delta_tdep_L = 0.d0
          delta_tdep_R = 0.d0
        end if

        if (mod(i, gopt % write_freq) == 0) then
          write(*, '(f6.1, A)') i*t_step, ' fs'
        end if

        call solve_with_rk(i*t_step)

        cspins = pack_vectors(Sx_classical, Sy_classical, &
                              Sz_classical)
        s_neq = spindensity(rho)

        call spindensity_eq(cspins, energy_llg, t=1.0, &
                            temperature=real(Temp), &
                            jsd=real(J_sd), spin_eq=espins, &
                            rho=rho_ozaki, solver=gopt%eq_solver)

        diff = (s_neq - espins)

        ! Turn on the LLG part only after t_end
        if (i > int(t_long/t_step) .and. &
            mod(i, llg_step) == 0 .and. &
            (gopt%run_llg .or. gopt%replay_llg)) then

            count_llg = count_llg + 1
            
            ! run llg
            if (gopt % run_llg) then
                call propagate_llg(cspins, diff, llg_param)                
                ! Overwrite the classical spins with precessing spins
                if (allocated(pr_spins)) then
                    do pind = 1, size(pr_spins)
                        call pr_spins(pind)%update(i*t_step)
                        cspins(pr_spins(pind)%i) = pr_spins(pind)%s
                    end do
                end if
            
            ! Overwrite cspins with file
            else
                call read_spins(i*t_step, cspins, &
                                gopt%replay_cspins_file)
            end if
            ! update Sx/y/z_classical
            call unpack_vectors(cspins, Sx_classical, &
                                  Sy_classical, Sz_classical)
        end if
        ! update Hamiltonian
        if (allocated(pr_spins)) then
            do pind = 1, size(pr_spins)
                call pr_spins(pind)%update(i*t_step)
                cspins(pr_spins(pind)%i) = pr_spins(pind)%s
            end do
        end if
        call unpack_vectors(cspins, Sx_classical, &
                              Sy_classical, Sz_classical)
        call create_H(i*t_step, Ax_t(i))
        
        ! --------------------------------------
        ! Write output
        ! --------------------------------------

        if (gopt % save_spindensity_eq .and. &
            mod(i, gopt % write_freq) == 0) &

          call save_spins(i*t_step, espins, &
                          gopt%output_path // &
                          gopt%spindensity_eq_file)

        if (gopt % save_spindensity_neq .and. &
            mod(i, gopt % write_freq) == 0) &

          call save_spins(i*t_step, s_neq, &
                          gopt%output_path // &
                          gopt%spindensity_neq_file)

        if (gopt % save_cspins .and. &
            mod(i, gopt % write_freq) == 0) &

          call save_spins(i*t_step, cspins, &
                          gopt%output_path // &
                          gopt%cspins_file)

        if (mod(i, gopt%write_freq) == 0 .and. &
            gopt%save_charge_current) then

          call save_rho_and_chargecurr(i*t_step, &
                  gopt%output_path // &
                  gopt%charge_current_file)
        end if

        if (mod(i, gopt % write_freq) == 0 .and. &
            gopt%save_spin_current) then

          call save_spincurrents(i*t_step, &
                  gopt%output_path // &
                  gopt%spin_current_file)
        end if

        if (mod(i, gopt%write_freq) == 0 .and. &
            gopt%save_bond_currents) then

          call save_bond_currents(i*t_step, &
                  gopt%output_path // &
                  gopt%bond_currents_file)
        end if

        if (mod(i, gopt%write_freq) == 0 .and. &
            gopt%save_rho) then

          call save_rho(i*t_step, &
                        gopt%output_path // gopt%rho_file)
        end if

        if (mod(i, 500) == 0) then
          if (gopt % save_rkvec_end) then
              call save_rk_vector(rkvec, &
                       gopt%rkvec_file_path // &
                       gopt%save_rkvec_end_file)
          end if
        end if

    end do

    call SYSTEM_CLOCK(time_2, cot_rate, cot_max)
    write(*,*) 'elapsed real time: ', &
             real(time_2-time_1) / real(cot_rate)

    contains

    function cmd_arg(arg_index)
    integer, intent(in) :: arg_index
    character (len=:), allocatable :: cmd_arg
    integer :: arg_length
    call get_command_argument(arg_index, length=arg_length)
    allocate(character(arg_length) :: cmd_arg)
    call get_command_argument(arg_index, value=cmd_arg)
    end function cmd_arg


    subroutine save_rk_vector(rk_vector, filename)
    character (len=*), intent(in) :: filename
    double complex, dimension(:), intent(in) :: rk_vector
    integer :: i

    open(unit=35, file=filename, status='replace', &
         action='write')
    call matrix_to_vec_to_save_end(rk_vector)
    do i = 1, size(rk_vector, 1)
        write(35, '(2es16.8)') real(rk_vector(i)), &
                               aimag(rk_vector(i))
    end do
    close(35)
    end subroutine save_rk_vector


    subroutine read_bias_file(filename, delta_L, delta_R)
    character (len=*), intent(in) :: filename
    double precision, dimension(:), &
        intent(inout) :: delta_L, delta_R
    integer :: i

    open(unit=35, file=filename, &
         status='unknown', action='read')

    do i = 1, size(delta_L, 1)
        read(35, *) delta_L(i), delta_R(i)
    end do

    end subroutine read_bias_file


end program tdnegf_1D_spinfull
