SHELL := /bin/bash
compiler = ifort
comp_options = -O2 -march=native -qopenmp -mkl \
               -lgfortran -heap-arrays -c
exe_options = -O2 -march=native -qopenmp -mkl \
              -lgfortran -heap-arrays
llg_ozaki_group =  obj/green_1d.o obj/llg.o \
                   obj/ozaki_integrator.o \
				   obj/math.o obj/constants.o \
				   obj/input_output.o \
				   obj/precess.o

test_files = test/test_llg
				   
end_comp = -I obj/
git_hash=$(shell git log --pretty=format:'%H' -n 1)

all: tdnegf_llg $(test_files) 


tdnegf_llg: obj/tdnegf_llg.o \
            obj/math.o obj/constants.o \
            obj/green_1d.o obj/llg.o \
	        obj/ozaki_integrator.o \
			obj/global_variables.o \
	        obj/create_csi.o \
			obj/create_Gam.o \
			obj/create_H.o obj/create_hi.o \
	        obj/ozaki.o obj/other.o \
			obj/matrix_to_vector_parallel.o \
	        obj/input_output.o \
	        obj/matrix_to_vec_to_save_end.o \
			obj/eom_omp.o obj/save_output.o \
	        obj/get_parameters.o obj/kron_prod.o \
			obj/set_initial_values.o \
	        obj/set_initial_values.o \
			obj/save_output.o  obj/rk_variables.o \
	        obj/rksuite.o obj/rk_solve.o \
			obj/vector_to_matrix_parallel.o \
	        obj/precess.o  obj/config.o
	@ echo "Building tdnegf_llg"
	@ echo "character(len=*), parameter :: git_hash = '$(git_hash)'" > \
	  git_version.inc
	@$(compiler) $(exe_options) obj/*o -o tdnegf_llg -llapack -lblas 
	@ rm -f git_version.inc

obj/tdnegf_llg.o:	tdnegf_llg.f90 obj/math.o \
    obj/constants.o \
    obj/green_1d.o obj/llg.o obj/ozaki_integrator.o \
	obj/global_variables.o \
	obj/create_csi.o  obj/create_Gam.o \
	obj/create_H.o obj/create_hi.o \
	obj/ozaki.o obj/other.o obj/matrix_to_vector_parallel.o \
	obj/input_output.o \
	obj/matrix_to_vec_to_save_end.o \
	obj/eom_omp.o obj/save_output.o \
	obj/get_parameters.o obj/kron_prod.o \
	obj/set_initial_values.o \
	obj/set_initial_values.o  obj/save_output.o \
	obj/rk_variables.o \
	obj/rksuite.o obj/rk_solve.o \
	obj/vector_to_matrix_parallel.o \
	obj/precess.o obj/config.o
	@ echo "Building tdnegf_llg.o"
	@ echo \
	"character(len=*), parameter :: git_hash = '$(git_hash)'" > \
	  git_version.inc
	@$(compiler) $(comp_options) tdnegf_llg.f90  $(end_comp) 
	@ rm -f git_version.inc
	@ mv tdnegf_llg.o obj/
	

obj/math.o: src/math.f90
	@echo "Building math.o"
	@$(compiler) $(comp_options) src/math.f90
	@mv math.o obj/
	@mv math.mod obj/


obj/constants.o:  src/constants.f90 obj/math.o 
	@echo "Building constants.o"
	@$(compiler) $(comp_options) src/constants.f90 $(end_comp) 
	@mv constants.o obj/
	@mv constants.mod obj/


obj/green_1d.o: src/green_1d.f90 obj/constants.o obj/math.o obj/global_variables.o
	@echo "Building green_1d.o"
	@$(compiler) $(comp_options) src/green_1d.f90 $(end_comp)
	@mv green_1d.o obj/
	@mv green_1d.mod obj/


obj/llg.o: src/llg.f90 obj/math.o obj/constants.o obj/input_output.o
	@echo "Building llg.o"
	@$(compiler) $(comp_options) src/llg.f90 $(end_comp)
	@mv llg.o obj/
	@mv llg.mod obj/

obj/precess.o: obj/llg.o src/precess.f90 obj/math.o
	@echo "Building precess.o"
	@$(compiler) $(comp_options) src/precess.f90 $(end_comp)
	@mv precess.o obj/
	@mv precess.mod obj/

obj/config.o: src/config.f90  obj/global_variables.o \
    obj/llg.o  obj/precess.o obj/math.o
	@echo "Building config.o"
	@$(compiler) $(comp_options) src/config.f90 $(end_comp)
	@mv config.o obj/
	@mv config.mod obj/


obj/ozaki_integrator.o: src/ozaki_integrator.f90 obj/math.o \
    obj/constants.o obj/green_1d.o
	@echo "Building ozaki_integrator.o"
	@$(compiler) $(comp_options) src/ozaki_integrator.f90 $(end_comp)
	@mv ozaki_integrator.o obj/
	@mv ozaki_integrator.mod obj/

obj/global_variables.o: src/global_variables.f90 obj/input_output.o
	@echo "Building global_variables.o"
	@$(compiler) $(comp_options) src/global_variables.f90 $(end_comp)
	@mv global.mod obj/
	@mv global_variables.o obj/


obj/create_csi.o: src/create_csi.f90 obj/global_variables.o
	@echo "Building create_csi.o"
	@$(compiler) $(comp_options) src/create_csi.f90 $(end_comp)
	@mv create_csi.o obj/


obj/create_Gam.o: src/create_Gam.f90 obj/global_variables.o
	@echo "Building create_Gam.o"
	@$(compiler) $(comp_options) src/create_Gam.f90 $(end_comp)
	@mv create_Gam.o obj/


obj/create_H.o: src/create_H.f90 obj/global_variables.o
	@echo "Building create_H.o"
	@$(compiler) $(comp_options) src/create_H.f90 $(end_comp)
	@mv create_H.o obj/


obj/create_hi.o: src/create_hi.f90 obj/global_variables.o
	@echo "Building create_hi.o"
	@$(compiler) $(comp_options) src/create_hi.f90 $(end_comp)
	@mv create_hi.o obj/


obj/eom_omp.o: src/eom_omp.f90 obj/global_variables.o
	@echo "Building eom_omp.o"
	@$(compiler) $(comp_options) src/eom_omp.f90 $(end_comp)
	@mv eom_omp.o obj/


obj/get_parameters.o: src/get_parameters.f90 obj/global_variables.o
	@echo "Building get_parameters.o"
	@$(compiler) $(comp_options) src/get_parameters.f90 $(end_comp)
	@mv get_parameters.o obj/


obj/kron_prod.o: src/kron_prod.f90
	@echo "Building kron_prod.o"
	@$(compiler) $(comp_options) src/kron_prod.f90
	@mv kron_prod.o obj/


obj/matrix_to_vector_parallel.o: src/matrix_to_vector_parallel.f90 \
        obj/global_variables.o
	@echo "Building matrix_to_vector_parallel.o"
	@$(compiler) $(comp_options) src/matrix_to_vector_parallel.f90 $(end_comp)
	@mv matrix_to_vector_parallel.o obj/


obj/matrix_to_vec_to_save_end.o: src/matrix_to_vec_to_save_end.f90 \
	    obj/global_variables.o
	@echo "Building matrix_to_vec_to_save_end.o"
	@$(compiler) $(comp_options) src/matrix_to_vec_to_save_end.f90 $(end_comp)
	@mv matrix_to_vec_to_save_end.o obj/

	
obj/other.o: src/other.f90 obj/global_variables.o
	@echo "Building other.o"
	@$(compiler) $(comp_options) src/other.f90 $(end_comp)
	@mv other.o obj/ 
	

obj/ozaki.o: src/ozaki.f90 obj/global_variables.o
	@echo "Building ozaki.o"
	@$(compiler) $(comp_options) src/ozaki.f90 $(end_comp)
	@mv ozaki.o obj/ 


obj/set_initial_values.o: src/set_initial_values.f90 obj/global_variables.o
	@echo "Building set_initial_values.o"
	@$(compiler) $(comp_options) src/set_initial_values.f90 $(end_comp)
	@mv set_initial_values.o obj/


obj/save_output.o: src/save_output.f90 obj/global_variables.o
	@echo "Building save_output.o"
	@$(compiler) $(comp_options) src/save_output.f90 $(end_comp)
	@mv save_output.o obj/


obj/vector_to_matrix_parallel.o: src/vector_to_matrix_parallel.f90 \
	  obj/global_variables.o
	@echo "Building vector_to_matrix_parallel.o"
	@$(compiler) $(comp_options) src/vector_to_matrix_parallel.f90 $(end_comp)
	@mv vector_to_matrix_parallel.o obj/

obj/input_output.o: src/input_output.f90  obj/math.o
	@echo "Building input_output.o"
	@$(compiler) $(comp_options) src/input_output.f90 $(end_comp)
	@mv input_output.o obj/
	@mv input_output.mod obj/

obj/rk_variables.o: src/rk_variables.f90 
	@echo "Building rk_variables.o"
	@$(compiler) $(comp_options) src/rk_variables.f90
	@mv rksuite_vars.mod obj/
	@mv rk_variables.o obj/

obj/rksuite.o: src/rksuite.f 
	@echo "Building rksuite.o"
	@$(compiler) $(comp_options) src/rksuite.f 
	@mv rksuite.o obj/
	
	
obj/rk_solve.o: src/rk_solve.f90 obj/global_variables.o obj/rk_variables.o \
	  obj/rksuite.o
	@echo "Building rk_solve.o"
	@$(compiler) $(comp_options) src/rk_solve.f90 $(end_comp)
	@mv rk_solve.o obj/

test/test_llg: test/test_llg.f90 obj/llg.o obj/math.o obj/input_output.o \
               obj/input_output.o
	@echo "Building test_llg"
	@$(compiler) $(exe_options) -o test_llg  \
	 -I obj obj/math.o obj/constants.o obj/llg.o \
	 obj/input_output.o test/test_llg.f90 
	@mv test_llg test/

test/test_ozaki: test/test_ozaki.f90 obj/ozaki_integrator.o obj/green_1d.o
	@ echo "Building test_ozaki"
	@$(compiler) $(exe_options) -o test_ozaki \
	  -I obj $(llg_ozaki_group) test/test_ozaki.f90
	@mv test_ozaki test/

.PHONY: test
test: 
	@echo "Running test_llg"; ./test/test_llg; echo "Done"
	@echo "Running test_ozaki"; ./test/test_ozaki; echo "Done"


.PHONY: build 
build: 
	@status=`git status --porcelain`; echo "'$$status'"; \
	 if [ -z "$$status" ]; then rm obj/tdnegf_llg.o; make; else echo "Error! Commit first"; fi; 
	

clean:
	@rm -f obj/*o obj/*mod tdnegf_llg
	@rm -f src/*o src/*mod



