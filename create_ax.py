#!/usr/bin/env python3
import numpy as np


def main():

    t_start = 0
    lamda   = 5

    tstep = 0.1
    tend  = 5
    amp   = 0

    tend    /= tstep
    t_start /= tstep
    lamda   /= tstep
    

    delta_L = np.zeros((int(tend),1))
    delta_R = np.zeros((int(tend),1))

    delta_L[int(t_start): \
            int(t_start + lamda)] = amp*0.001 # in meV

    
    delta = np.concatenate((delta_L, delta_L), axis = 1)


    with open('light_Ax.txt', 'w+') as datafile_id:
        np.savetxt(datafile_id, delta, fmt = ['%.3f', '%.3f'])
    
if __name__ == main():
    main()
